<?php
get_header();
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    queryForVisaRequirements();
    ?>
    <body>
    <?php
    if (have_rows('requirements_page_data')):
        ?>
        <div class="china-visa">
            <!-- Start Container  -->
            <div class="container">
                <?php while (have_rows('requirements_page_data')):
                    the_row(); ?>
                    <?php if (get_row_layout() == 'visa_and_destination_info_section') { ?>
                    <div class="visa-required">
                        <div class="make-flex">
                            <div class="flex-parent">
                                <h3><?php the_sub_field('visa_title'); ?>
                                    <span><?php the_sub_field('start_your_visa_request_now'); ?></span>
                                </h3>
                                <?php if (have_rows('destination_and_visa_info')): while (have_rows('destination_and_visa_info')): the_row(); ?>
                                    <p><strong><?php the_sub_field('destination_visa_info'); ?>:</strong> <?php the_sub_field('visa_info'); ?></p>
                                <?php endwhile; endif; ?>
                                <div class="links">
                                    <ul>
                                        <?php
                                         if(have_rows('get_visa__email_pdf__view_pdf_buttons')){
                                        while(have_rows('get_visa__email_pdf__view_pdf_buttons')){
                                        the_row();
                                        if(have_rows('view_application_kit_link')){
                                             while(have_rows('view_application_kit_link')){
                                                 the_row();
                                         $link_or_file=get_sub_field('link_or_file');
                                        if($link_or_file=='file'){
                                        $view_application_link=get_sub_field('pdf_file');
                                        }else{
                                            $view_application_link=get_sub_field('pdf_link');
                                        }
                                        $target=get_sub_field('target');
                                      ?>
                                        <li><span><i class="far fa-eye"></i></span><a href="<?php echo $view_application_link; ?>" target="<?php echo $target; ?>">View Application Kit </a></li>
                                        <?php
                                    }
                                    }
                                        if(have_rows('email_application_button')){
                                            while(have_rows('email_application_button')){
                                            the_row();
                                        $email_application_link=get_sub_field('pdf_to_be_sent');
                                        $mail_title=get_sub_field('mail_title');
                                        $mail_subject=get_sub_field('mail_subject');
                                        ?>
                                        <li><span><i class="fas fa-envelope"></i></span><a id="send-mail-toggle">Email Application Kit</a></li>
                                <?php } } } } ?>
                                </div>
                                <form name='email-header' method="post">
                                <div class="sending-email-div row" style="display:none">
                                    <input  placeholder="Email" id="email-input" name="sender" type="email" required>
                                    <input type="submit" name="submit-mail-header" value="Send Email" class="red-button">
                                </div>
                                </form>
                                <?php
                                if(isset($_POST['submit-mail-header'])){
                                    $attachments = get_attached_file( $email_application_link['id'] );
                                    $to=$_POST['sender'];
                                    $subject=$mail_title;
                                    $message=$mail_subject;
                                    $headers=$mail_title;
                                    $mailresult=false;
                                    $mailresult=wp_mail( $to, $subject, $message, $headers,$attachments );
                                    if($mailresult==true){
                                    echo '<p class="mail-sent-successfully">Mail Sent Successfully <i class="fas fa-check"></i></p>';
                                }
                                }

                                ?>
                            </div>
                            <?php
                            if(have_rows('get_visa__email_pdf__view_pdf_buttons')){
                            while(have_rows('get_visa__email_pdf__view_pdf_buttons')){
                            the_row();
                            if(have_rows('get_visa_button')){
                            while(have_rows('get_visa_button')){
                            the_row();
                            ?>
                            <div class="flex-parent">
                                <?php $get_visa_button = get_sub_field('button_link'); ?>
                                <a class="red-button" href="<?php echo $get_visa_button['url']; ?>"
                                   target="<?php echo $get_visa_button['target']; ?>"><?php the_sub_field('button_title'); ?>
                                    <i class="fa fa-chevron-right" aria-hidden="true" style="padding-left:5px;"></i></a>
                            </div>
                            <?php
                            } } } }
                            ?>
                        </div>
                    </div>
                    <!-- End visa-required -->
                <?php } ?>
                    <?php if (get_row_layout() == 'requirements_boxes_section') { ?>
                    <h1>The Information You Will Need For Your Visa</h1>

                    <div class="information">
                        <ul class="make-flex">
                            <!-- Flex-Child -->
                            <?php if (have_rows('requirement_box')):
                                while (have_rows('requirement_box')):
                                    the_row();
                                    ?>
                                    <li class="child-flex">
                                        <h5><?php the_sub_field('requirement_title'); ?></h5>
                                        <img src="<?php the_sub_field('requirement_image'); ?>">
                                        <div class="show">
                                            <div class="learn-more">
                                                <p>Learn More <i class="fa fa-chevron-down" aria-hidden="true" style="padding-left:5px;"></i></p>
                                            </div>
                                            <div class="show-hide">
                                                <h6><?php the_sub_field('requirement_title'); ?></h6>
                                                <?php the_sub_field('requirement_description_floating_window'); ?>
                                            </div>
                                        </div>
                                    </li>
                                <?php endwhile; endif; ?>
                        </ul>
                    </div>
                    <!-- End information -->
                <?php } ?>
                    <?php if (get_row_layout() == 'consular_closings'): ?>
                    <div class="consular-closings">
                        <h1>Consular Closings</h1>
                        <hr>
                        <?php the_sub_field('consolar_description') ?>
                        <?php if (have_rows('service_fees_section')): ?>
                            <h1>Service Fees</h1>
                            <hr>
                            <?php while (have_rows('service_fees_section')): the_row(); ?>
                                <strong><?php the_sub_field('fees_title'); ?></strong>
                                <p> <?php the_sub_field('fees_description'); ?></p>
                                <?php if (have_rows('fee_line')): ?>
                                    <ul>
                                        <?php while (have_rows('fee_line')): ?>
                                            <?php the_row(); ?>
                                            <li>
                                                <p><?php the_sub_field('fee_title'); ?></p>
                                                <p><?php the_sub_field('fee_price'); ?></p>
                                            </li>
                                        <?php endwhile; ?>
                                    </ul>
                                <?php endif; ?>
                            <?php endwhile; endif; ?>
                        <div class="get-my-visa">
                            <ul>
                                <?php
                                if(have_rows('get_visa__email_pdf__view_pdf_buttons')){
                                while(have_rows('get_visa__email_pdf__view_pdf_buttons')){
                                the_row();
                                if(have_rows('view_application_kit_link')){
                                while(have_rows('view_application_kit_link')){
                                the_row();
                                $link_or_file=get_sub_field('link_or_file');
                                if($link_or_file=='file'){
                                    $view_application_link=get_sub_field('pdf_file');
                                }else{
                                    $view_application_link=get_sub_field('pdf_link');
                                }
                                $target=get_sub_field('target');
                                ?>
                                    <li><span><i class="far fa-eye"></i></span><a href="<?php echo $view_application_link; ?>" target="<?php echo $target; ?>">View Application Kit </a></li>
                                    <?php
                                }
                                }
                                    if(have_rows('email_application_button')){
                                        while(have_rows('email_application_button')){
                                            the_row();
                                            $email_application_link2=get_sub_field('pdf_to_be_sent');
                                            $mail_title2=get_sub_field('mail_title');
                                            $mail_subject2=get_sub_field('mail_subject');
                                            ?>
                                            <li><span><i class="fas fa-envelope"></i></span><a id="send-mail-toggle2">Email Application Kit</a></li>
                                        <?php } } } } ?>
                            </ul>
                                 <?php
                            if(have_rows('get_visa__email_pdf__view_pdf_buttons')){
                            while(have_rows('get_visa__email_pdf__view_pdf_buttons')){
                            the_row();
                            if(have_rows('get_visa_button')){
                            while(have_rows('get_visa_button')){
                            the_row();
                            ?>
                            <a class="visa-button" href="<?php echo $get_visa_button['url']; ?>" target="<?php echo $get_visa_button['target']; ?>"><?php the_sub_field('button_title'); ?> <i class="fa fa-chevron-right" aria-hidden="true" style="padding-left:5px;"></i></a>
                            <?php } } } } ?>
                        </div>
                        <form name="email-footer" method="post">
                            <div class="sending-email-div2 row" style="display:none">
                                <input  placeholder="Email" id="email-input" name="sender" type="email" required>
                                <input type="submit" value="Send Email" class="red-button send-email-btn" name="submit-mail-footer">
                            </div>
                        </form>
                        <?php
                        if(isset($_POST['submit-mail-footer'])){
                            $attachments2 = get_attached_file( $email_application_link2['id'] );
                            $to2=$_POST['sender'];
                            $subject2=$mail_title2;
                            $message2=$mail_subject2;
                            $headers2=$mail_title2;
                            $mailresult2=false;
                            $mailresult2=wp_mail( $to2, $subject2, $message2, $headers2,$attachments2 );
                            if($mailresult2==true){
                                echo '<p class="mail-sent-successfully">Mail Sent Successfully <i class="fas fa-check"></i></p>';
                            }
                        }

                        ?>
                    </div>
                <?php endif; ?>
                <?php endwhile; ?>
            </div>
        </div>
    <?php endif; ?>
    </body>
    <?php

    wp_reset_query();
?>

    <script>
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
        jQuery(document).on('click', '#send-mail-toggle', function() {
            jQuery('.sending-email-div').slideToggle();
        });

        jQuery(document).on('click', '#send-mail-toggle2', function() {
            jQuery('.sending-email-div2').slideToggle();
        });
    </script>
    <?php
}else{
    $home_url=get_home_url();
    ?>
<script>
    window.location.replace("<?php echo $home_url; ?>");
</script>
<?php
}
            get_footer();

