<?php
function visaCIBTFormShortcode() {
	?>
	<?php
	global $wpdb;
	$page_name = 'requirements';
	if (null === $wpdb->get_row("SELECT post_name FROM wp_posts WHERE post_name = '".$page_name."'", 'ARRAY_A')) {
		$requirements_page_path = get_home_url();
	} else {
		$requirements_page_path = get_permalink(get_page_by_path('requirements'));
	}
	?>
	<form method="POST" action="<?php echo $requirements_page_path; ?>">
		<div class="row visa-country-destination-select-row">
			<div class="visa-plugin-select-wrapper">
				<label>I Hold passport from:</label>
				<?php CategoriesMenuFormQuery('Countries'); ?>
			</div>
			<div class="load-state visa-plugin-select-wrapper">
				<label>State of Residence:</label>
				<select name="states" class="states">
					<option>Please select a country</option>
				</select>
			</div>
			<div class="Destination-selection visa-plugin-select-wrapper">
				<label>I am going to:</label>
				<?php CategoriesMenuFormQuery('Destination'); ?>
			</div>
			<div class="purpose-selection visa-plugin-select-wrapper">
				<label>My purpose of trip is:</label>
				<?php CategoriesMenuFormQuery('Purpose'); ?>
			</div>
			<div class="row visa-country-destination-select-row button-row">
			<input type="submit" class="auth-secondary-btn iv-wp-from-bottom" value="Submit">
            </div>
		</div>
	</form>
	<script type="text/javascript">
    var ajaxurl = '<?php echo admin_url("admin-ajax.php"); ?>';
    jQuery(document).ready(function () {
        setTimeout(function () {
        window.dodo.find('select:not(.in-modal).country-plugin').on('change', function () {
          var country = jQuery(this).val();
          console.log(country);
            if (country != '') {
            var data = {
              action: 'get_states_by_ajax',
              country: country,
              'security': '<?php echo wp_create_nonce("get-states"); ?>'
            };
         //     jQuery('.states').html('Loading...');
              jQuery.post(ajaxurl, data, function (response) {
              jQuery('.states').html(response);
            });
          } else {
            alert('not true');
          }
        });
      }, 300);
    });
	</script>
	<?php
	
}

add_shortcode('visarequirementsform', 'visaCIBTFormShortcode');