<?php
/*
 * Plugin Name: CIBT-Travel Visa
 * Description: This is a simple plugin to make you able to link destination with resident & generate a custom page with custom data easily.
 * Author: You Know me.
 * Plugin URI: #
 * Version : 1.1
 */

/* Added jQuery and some other scripts to our plugin */
wp_enqueue_script( 'jquery' );

function customScriptsforOurPlugin()
{
    wp_enqueue_script('custom-scripts', plugin_dir_url(__FILE__) . '../assets/js/front-end-scripts.js');
}
add_action( 'wp_enqueue_scripts', 'customScriptsforOurPlugin');

function customAdminDashboardScripts( $hook ) {
	wp_enqueue_script( 'admin-dashboard-custom-scripts', plugin_dir_url( __FILE__ ) . '../assets/js/scripts.js' );
}

add_action( 'admin_enqueue_scripts', 'customAdminDashboardScripts' );
/* END */

/* ADded some style and scripts for Requirements Page */

function AddCustomStyleAndScripts(){
    if (is_page('requirements')) {
        // Style CSS
        wp_enqueue_style('custom-font', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap', 'array', '', '');
        wp_enqueue_style('bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css', 'array', '', '');
        wp_enqueue_style('section-css', plugins_url('../assets/css/section.css',__FILE__), 'array', '', '');
        wp_enqueue_script('section-scripts',plugins_url('../assets/css/section.css',__FILE__));
        wp_enqueue_script('jquery','https://code.jquery.com/jquery-3.2.1.slim.min.js');
        wp_enqueue_script('popper','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js');
        wp_enqueue_script('bootstrap','https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js');
        wp_enqueue_script('fontawesome-js','https://kit.fontawesome.com/dff06bf6db.js');
    }
}
add_action( 'wp_enqueue_scripts', 'AddCustomStyleAndScripts',99);

// Added some style to Admin dashboard

/* END */

/* This is how to get select menus for Countries, Destination and purpose */

function CategoriesMenuFormQuery( $category ) {
	?>
    <select name="<?php echo $category; ?>" class="<?php if($category=='Countries'){echo "country-plugin"; } ?>" id="<?php if($category=='Countries'){echo "country-plugin"; } ?>" required>
		<option value="">-</option>
        <?php
		$terms = get_terms( array(
			'taxonomy'   => $category,
			'hide_empty' => false,
		) );
		foreach ( $terms as $term ) {
			echo "<option value='" . $term->term_id . "'>" . $term->name . "</option>";
		}
		?>
    </select>
	<?php
}
/* End */


// Ajax call for states
add_action( 'wp_ajax_get_states_by_ajax', 'get_states_by_ajax_callback' );
add_action( 'wp_ajax_nopriv_get_states_by_ajax', 'get_states_by_ajax_callback' );
function get_states_by_ajax_callback() {
	$chosen_state = $_POST['country'];

	$args   = array(
		'taxonomy'   => 'states_taxonomy',
		'hide_empty' => false,
		'meta_query' => array(
			array(
				'key'     => 'country_state',
				'value'   => $chosen_state,
				'compare' => '='
			)
		)
	);
	$states = get_terms( $args );
	?>
<!--    <label>State of Residence:</label>-->
<!--    <select name="states" class="states" required>-->
	<?php
	if ( $states ) {
		foreach ( $states as $state ) {
			echo "<option value='" . $state->term_id . "'>" . $state->name . "</option>";
		}
		?>
<!--        </select>-->
		<?php
	} else {
		echo "<option>-</option>";
	}
	exit;
}
/* End */

    function queryForVisaRequirements(){
        $Country= $_POST['Countries'];
        $state= $_POST['states'];
        $destination=$_POST['Destination'];
        $purpose=$_POST['Purpose'];

        $tax_query = array('relation' => 'AND');
        if(!empty($Country)){
            $tax_query[]= array(
                'taxonomy' => 'Countries',
                'field' => 'term_id',
                'terms' => $Country
            );
        }
        if(!empty($state)){
            $tax_query[]= array(
                'taxonomy' => 'states_taxonomy',
                'field' => 'term_id',
                'terms' => $state
            );
        }
	    if(!empty($destination)){
		    $tax_query[]= array(
			    'taxonomy' => 'Destination',
			    'field' => 'term_id',
			    'terms' => $destination,
		    );
	    }
	    if(!empty($purpose)){
		    $tax_query[]= array(
			    'taxonomy' => 'Purpose',
			    'field' => 'term_id',
			    'terms' => $purpose,
		    );
	    }
        $query = new WP_Query(
            array(
                'post_type' => 'visa_requirements',
                'posts_per_page' => '-1',
                //Filter taxonomies by id's passed from course finder widget
                'tax_query' => $tax_query,
            )
        );

        // The Loop
        if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
                $query->the_post();

            }
        }else{
            echo "No Posts!";
        }

    }
