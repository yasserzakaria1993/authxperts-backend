 jQuery(document).ready( function () {
        jQuery('form#post').find('.categorychecklist input').each(function() {
            var new_input = jQuery('<input type="radio" />'),
                attrLen = this.attributes.length;

            for (i = 0; i < attrLen; i++) {
                if (this.attributes[i].name != 'type') {
                    new_input.attr(this.attributes[i].name.toLowerCase(), this.attributes[i].value);
                }
            }

            jQuery(this).replaceWith(new_input);
        });
    });
