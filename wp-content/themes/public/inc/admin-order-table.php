<?php
add_action('admin_menu', function () {
	add_submenu_page(
		'edit.php?post_type=orders',
		'Orders Table',
		'Orders Table',
		'manage_options',
		'orders_table',
		'orders_table_callback'
	);
});

function orders_table_callback() {
	include "admin-table-content.php";
}