<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style>
	#table-wrap #table_filter{
		text-align: left;
		margin-bottom: 20px;
	}

	#table-wrap #table_filter input{
		border: 1px solid #ddd;
		margin-top: 10px;
		font-size: 15px;
		padding: 5px;
	}

	#table thead th{
		font-size: 13px;
		text-align: center;
	}

	#table tbody tr td{
		font-size: 13px;
		text-align: center;
	}

	.dt-buttons{
		float: left;
		margin-top: 15px;
	}
</style>
<div class="wrap">
	<h1>Orders Table</h1>
	<div class="table-page-wrapper">
		<div class="table_wrapper">
			<div class="container">
				<div id="table-wrap">

					<table id="table" class="table table-striped table-bordered" style="width:100%">
						<thead>
							<tr>
							<th>Full Name</th>
							<th>Quoted Amount</th>
							<th>Donation Amount</th>
							<th>Country</th>
							<th>Postal Code</th>
							<th>Email</th>
							<th>Phone Number</th>
						</tr>
						</thead>
						<tbody>
							<?php
							$orders = new WP_Query([
								'post_type'    => 'orders',
								'posts_per_page'    => -1
							]);
							while ($orders->have_posts()) : $orders->the_post(); ?>
								<tr>
									<td><?php echo get_post_meta(get_the_ID(),'full_name', true) ?></td>
									<td><?php echo get_post_meta(get_the_ID(),'order_quoted_amount', true) ?>$</td>
									<td><?php echo get_post_meta(get_the_ID(),'donation_amount', true) ?>$</td>
									<td><?php echo get_post_meta(get_the_ID(),'country', true) ?></td>
									<td><?php echo get_post_meta(get_the_ID(),'postal_code', true) ?></td>
									<td><?php echo get_post_meta(get_the_ID(),'email', true) ?></td>
									<td><?php echo get_post_meta(get_the_ID(),'phone_number', true) ?></td>
								</tr>
							<?php endwhile; wp_reset_query() ?>
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>

<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>


<script>
    (function ($) {
        var editor; // use a global for the submit and return data rendering in the examples
        $('#table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    })(jQuery);
</script>
