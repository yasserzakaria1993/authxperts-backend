<?php
require_once( get_template_directory() . '/sdk/vendor/autoload.php' );

//for sandbox
const MERCHANT_LOGIN_ID = "6h8x3SVb";
const MERCHANT_TRANSACTION_KEY = "534dAbZu752sLSrq";

//const MERCHANT_LOGIN_ID = "8DgpP65x";
//const MERCHANT_TRANSACTION_KEY = "2st98ZNz6sda2N99";

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

define("AUTHORIZENET_LOG_FILE", "phplog");

function chargeCreditCard($card_number,$ex_m,$ex_y,$cvv,$amount)
{

}

add_action( 'wp_ajax_os_payment_check', 'os_payment_check' );
add_action( 'wp_ajax_nopriv_os_payment_check', 'os_payment_check' );
function os_payment_check() {


	if ( !empty( $_POST['card-number'] ) && !empty($_POST['expiration-m']) && !empty($_POST['expiration-y']) && !empty($_POST['cvv']) && !empty($_POST['total'])) {
		$card_number = sanitize_text_field( $_POST['card-number'] );
		$ex_m = sanitize_text_field( $_POST['expiration-m'] );
		$ex_y = sanitize_text_field( $_POST['expiration-y'] );
		$cvv = sanitize_text_field( $_POST['cvv'] );
		$amount = sanitize_text_field($_POST['total']);

		$first_name = sanitize_text_field($_POST['fname']);
		$last_name = sanitize_text_field($_POST['lname']);
		$address = sanitize_text_field($_POST['address']);
		$city = sanitize_text_field($_POST['city']);
		$zip = sanitize_text_field($_POST['postal-code']);
		$country = sanitize_text_field($_POST['country']);
		$state = !empty($_POST['state']) ? sanitize_text_field($_POST['state']) : 'not found';
		$email =  sanitize_text_field($_POST['b-email']) ;



		/* Create a merchantAuthenticationType object with authentication details
	   retrieved from the constants file */
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);

		// Set the transaction's refId
		$refId = 'ref' . time();

		// Create the payment data for a credit card
		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($card_number);
		$creditCard->setExpirationDate("$ex_y-$ex_m");
		$creditCard->setCardCode($cvv);

		// Add the payment data to a paymentType object
		$paymentOne = new AnetAPI\PaymentType();
		$paymentOne->setCreditCard($creditCard);

		// Create order information
		$order = new AnetAPI\OrderType();
		$order->setInvoiceNumber("10101");
		$order->setDescription("authxperts Order");

		// Set the customer's Bill To address
		$customerAddress = new AnetAPI\CustomerAddressType();
		$customerAddress->setFirstName($first_name);
		$customerAddress->setLastName($last_name);
		$customerAddress->setCompany("not found");
		$customerAddress->setAddress($address);
		$customerAddress->setCity($city);
		$customerAddress->setState($state);
		$customerAddress->setZip($zip);
		$customerAddress->setCountry($country);

		// Set the customer's identifying information
		$customerData = new AnetAPI\CustomerDataType();
		$customerData->setType("individual");
		$customerData->setId(time());
		$customerData->setEmail($email);

		// Add values for transaction settings
		$duplicateWindowSetting = new AnetAPI\SettingType();
		$duplicateWindowSetting->setSettingName("duplicateWindow");
		$duplicateWindowSetting->setSettingValue("60");


		// Create a TransactionRequestType object and add the previous objects to it
		$transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType("authCaptureTransaction");
		$transactionRequestType->setAmount($amount);
		$transactionRequestType->setOrder($order);
		$transactionRequestType->setPayment($paymentOne);
		$transactionRequestType->setBillTo($customerAddress);
		//$transactionRequestType->setCustomer($customerData);
		$transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
		//$transactionRequestType->addToUserFields($merchantDefinedField1);
		//$transactionRequestType->addToUserFields($merchantDefinedField2);

		// Assemble the complete transaction request
		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setTransactionRequest($transactionRequestType);

		// Create the controller and get the response
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);


		if ($response != null) {
			// Check to see if the API request was successfully received and acted upon
			if ($response->getMessages()->getResultCode() == "Ok") {
				// Since the API request was successful, look for a transaction response
				// and parse it to display the results of authorizing the card
				$tresponse = $response->getTransactionResponse();

				if ($tresponse != null && $tresponse->getMessages() != null) {
//					echo " Successfully created transaction with Transaction ID: " . $tresponse->getTransId() . "\n";
//					echo " Transaction Response Code: " . $tresponse->getResponseCode() . "\n";
//					echo " Message Code: " . $tresponse->getMessages()[0]->getCode() . "\n";
//					echo " Auth Code: " . $tresponse->getAuthCode() . "\n";
//					echo " Description: " . $tresponse->getMessages()[0]->getDescription() . "\n";
					echo "success";
				} else {
					echo "Transaction Failed 1 \n";
					if ($tresponse->getErrors() != null) {
						echo " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
					}
				}
				// Or, print errors if the API request wasn't successful
			} else {
				$tresponse = $response->getTransactionResponse();
				if ($tresponse != null && $tresponse->getErrors() != null) {
					echo  $tresponse->getErrors()[0]->getErrorText() ;
				} else {
					echo $response->getMessages()->getMessage()[0]->getText() ;
				}
			}
		} else {
			echo  "No response returned";
		}

		//return $response;


	}else{
		echo "Error,Please make sure that the credit card data is correct";
	}



	die();
}