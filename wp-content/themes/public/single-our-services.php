<?php
get_header();
?>

<div class="services-single-page-wrapper">
	<div class="page-cover">
		<?php
		// first, get the image ID returned by ACF
		$image_id = get_field('cover_image');
		// and the image size you want to return
		$image_size = 'full';
		// use wp_get_attachment_image_src to return an array containing the image
		// we'll pass in the $image_id in the first parameter
		// and the image size registered using add_image_size() in the second
		$image_array = wp_get_attachment_image_src($image_id, $image_size);
		// finally, extract and store the URL from $image_array
		$image_url = $image_array[0];
		?>
		<img alt="Services Cover" class="image-cover no-zoom" src="<?php echo $image_url; ?>">
		<div class="container">
			<p class="auth-page-cover-subtitle iv-wp-from-left"><span class="square-cover"></span><?php the_field('cover_top_word') ?></p>
			<h1 class="page-cover-main-title iv-wp-from-top"><?php the_field('cover_main_text') ?></h1>
		</div>
	</div>
	<div class="services-single-content">
		<div class="container">
			<div class="service-item-content iv-wp-from-bottom ">
				<?php if (have_rows('tab_content')) : $num = 1; ?>
					<?php while (have_rows('tab_content')) : the_row()?>

						<?php if (get_row_layout() == 'countries_select')  : ?>
							<select class="countries iv-wp-from-bottom" id="" name="country">
								<?php if (have_rows('countries_block')) : $num = 1; ?>
									<?php while (have_rows('countries_block')) : the_row()?>
										<option value="<?php the_sub_field('country_code') ?>"><?php the_sub_field('country_name') ?></option>
									<?php endwhile; ?>
								<?php endif; ?>
							</select>
						<?php endif; ?>

						<?php if (get_row_layout() == 'countries_list_with_flag')  : ?>
							<h2 class="service-main-title auth-secondary-color"><?php the_sub_field('block_title') ?></h2>
							<div class="countries-flags">
								<div class="row">
									<?php if (have_rows('countries_list')) : $num = 1; ?>
										<?php while (have_rows('countries_list')) : the_row()?>
											<div class="col-xl-2 col-lg-3 col-sm-4 col-6">
												<div class="country-item iv-wp-from-bottom">
													<a href="<?php the_sub_field('link') ?>">
														<img alt="Flag" src="<?php the_sub_field('country_flag') ?>">
														<h4 class="country-title"><?php the_sub_field('country_name') ?></h4>
													</a>
												</div>
											</div>
										<?php endwhile; ?>
									<?php endif; ?>
								</div>
							</div>
						<?php endif; ?>

						<?php if (get_row_layout() == 'gallery')  : ?>
							<h2 class="service-main-title auth-secondary-color iv-wp-from-bottom"><?php the_sub_field('gallery_block_title') ?></h2>
							<div class="continent-section">
								<div class="container">
									<div class="row">
										<?php if (have_rows('images')) : $num = 1; ?>
											<?php while (have_rows('images')) : the_row()?>
												<div class="col-lg-4 col-sm-6 col-12">
													<div class="continent-item iv-wp-from-left">
														<a href="<?php the_sub_field('link') ?>">
															<img alt="Continent Img" src="<?php the_sub_field('image') ?>">
															<div class="continent-title"><?php the_sub_field('image_title') ?></div>
														</a>
													</div>
												</div>
											<?php endwhile; ?>
										<?php endif; ?>
									</div>
								</div>
							</div>
						<?php endif; ?>

						<?php if (get_row_layout() == 'text_block')  : ?>
							<?php if (have_rows('content')) : $num = 1; ?>
								<?php while (have_rows('content')) : the_row()?>
									<h4 class="auth-small-title-typography iv-wp"><?php the_sub_field('title') ?></h4>
									<p class="auth-body-text-typography iv-wp "><?php the_sub_field('text') ?></p>
									<?php if (!empty(get_sub_field('link'))) : ?>
										<a class="auth-link-typography-with-arrow iv-wp-from-bottom" href="<?php echo get_sub_field('link')['url']?>"><?php echo get_sub_field('link')['title']?></a>
									<?php endif; ?>
								<?php endwhile; ?>
							<?php endif; ?>
						<?php endif; ?>

					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>
