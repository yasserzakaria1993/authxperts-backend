<!doctype html>
<!--suppress CheckEmptyScriptTag -->
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<?php wp_head() ?>
	<style>
		
		
		header{
			background: <?php the_field('primary_color','option') ?>!important;
		}
		@keyframes animate {50% {-webkit-transform: scaleY(1); transform: scaleY(1);background:  <?php the_field('primary_color','option') ?>!important; } }
		@-webkit-keyframes animate {50% {-webkit-transform: scaleY(1); transform: scaleY(1);background:  <?php the_field('primary_color','option') ?>!important; } }
		.auth-filter-gradient-color, .about-page-wrapper section.about-hero .locations-container .location{
			background-image: linear-gradient(to left, <?php the_field('primary_color','option') ?> 0%, rgba(5, 105, 130, 0) 100%);
		}
		
		.home-page-wrapper section.simple-steps-section .place-order-steps-container .steps .step .number,footer.auth-footer .footer-social-icons .the-icons .the-icon,footer.auth-footer .footer-social-icons .the-title,footer.auth-footer .footer-bottom .d-flex .left-side p,footer.auth-footer .footer-links-title,footer.auth-footer .footer-bottom .d-flex .right-side .copyright-text,footer.auth-footer .footer-links .footer-link,footer.auth-footer .footer-bottom .d-flex .right-side a , .china-visa .visa-required p a, .china-visa .visa-required ul li a, .consular-closings .get-my-visa ul li a, .mail-sent-successfully i {
			color: <?php the_field('primary_color','option') ?>!important;
		}
		
		.auth-drop-down-wrapper .auth-drop-down,.place-order-page-wrapper section.place-order-details-container .place-order-steps-container .option,.place-order-page-wrapper section.place-order-details-container .place-order-steps-container .get-quote,.place-order-page-wrapper .place-order-details-container .content .payment-content,.place-order-page-wrapper .place-order-details-container .content .document-content,.services_wrapper .card .card-btn a,.services_wrapper .card:hover , .china-visa .visa-required .make-flex .red-button, .consular-closings .get-my-visa .visa-button, .visa-country-destination-select-row input{
			background: linear-gradient(to left, <?php the_field('second__color_1','option') ?> 0%, <?php the_field('second__color_2','option') ?> 100%)!important;
		}
		
		.home-page-wrapper section.home-hero .locations-container .location:before,header nav.navbar .navbar-toggler span,.about-page-wrapper section.about-hero .locations-container .location:before,.faqs-page-wrapper .faqs-sidebar-content .faqs-sidebar-item-content .the-question-body .a-character,.auth-tabs-sidebar .auth-sidebar-item.active:before,.page-cover .square-cover,.home-page-wrapper section.simple-steps-section .place-order-steps-container .steps .step .title .h-line,.home-page-wrapper section.simple-steps-section .place-order-steps-container .steps .step .title .icon .locator,#loading-center-absolute .object {
			background-color:<?php the_field('primary_color','option') ?>;
		}
		
		.auth-primary-border-color,footer.auth-footer .footer-bottom{
			border-color:<?php the_field('second__color_1','option') ?>!important;
		}
	.place-order-page-wrapper .place-order-details-container .content .payment-content:before ,.china-visa hr{
			border-bottom-color:<?php the_field('second__color_1','option') ?>!important;
		}
        .china-visa .visa-required{
            border-left:5px solid <?php the_field('second__color_1','option') ?>!important;
        }
		
		.auth-primary-color,.auth-main-super-title-typography, .auth-small-title-typography, .auth-link-typography-with-arrow, .auth-page-cover-subtitle, .auth-secondary-btn:hover, .auth-secondary-btn.active, .auth-tabs-sidebar-select-wrapper .select2-selection .select2-selection__arrow .fa-chevron-down, .place-order-page-wrapper section.place-order-details-container .place-order-steps-container .steps .step form .select2-selection .select2-selection__arrow .fa-chevron-down, .place-order-page-wrapper section.estimate-price .input-group .select2-selection .select2-selection__arrow .fa-chevron-down, .home-page-wrapper section.all-places .select2-selection .select2-selection__arrow .fa-chevron-down, .contact-content form .select2-selection .select2-selection__arrow .fa-chevron-down, .modal form .select2-selection .select2-selection__arrow .fa-chevron-down,header .auth-header-btn:hover, header .auth-header-btn.active,.auth-tabs-sidebar .auth-sidebar-item.active:after,.services_wrapper .card .card-head i,.blog-page-wrapper .blog-sidebar-content .blog-sidebar-item-content .article-date,.place-order-new-step-wrapper .right-col button,.place-order-new-step-wrapper .right-col button i,.place-order-page-wrapper section.place-order-details-container .text h3,.payment-option .the-title,.single-blog-page-wrapper .blog-article-content .article-date,.services_wrapper .card:hover .card-btn a, .china-visa .far, .china-visa .fa-envelope:before{
			color:<?php the_field('second__color_1','option') ?>!important;
		}
		
		.auth-main-gradient-color, .auth-primary-btn, .modal .close, .services-page-wrapper .services-slider .service-item.active, .place-order-page-wrapper section.estimate-price .estimated,
		.auth-search-form input, .services-page-wrapper .service-item-content .select2-selection, .services-page-wrapper .service-item-content .continent-section .continent-item,
		header .make-btn ,footer.auth-footer,#loading,.order-page-wrapper #order .alert, .send-email-btn,.make-btn-popup{
			background-image: linear-gradient(to left, <?php the_field('second__color_1','option') ?> 0%, <?php the_field('second__color_2','option') ?> 100%) !important;
		}


        .nav ul li.active .links:after{
            background: linear-gradient(to left, <?php the_field('second__color_1','option') ?> 0%, <?php the_field('second__color_2','option') ?> 100%) !important;
        }

        .nav ul .list:hover .links:after,.nav .add-ul,.nav ul .list .links:after{
            background: linear-gradient(to left, <?php the_field('second__color_1','option') ?> 0%, <?php the_field('second__color_2','option') ?> 100%) !important;
        }

        .nav .add-ul li:hover a{
            background-color:<?php the_field('primary_color','option') ?>;
        }

	</style>
<body <?php body_class() ?>>
<?php wp_body_open() ?>
<div id="loading">
	<div id="loading-center-absolute">
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
		<div class="object"></div>
	</div>
</div>
<header style="display: none;">
	<svg id="defs">
		<defs>
			<linearGradient gradientUnits="userSpaceOnUse" id="header-phone-gradient" x1="20.01" x2=".01" y1="10" y2="10.02">
				<stop offset="0" stop-color="<?php the_field('second__color_1','option') ?>"/>
				<stop offset="1" stop-color="<?php the_field('second__color_2','option') ?>"/>
			</linearGradient>
		</defs>
	</svg>
	
	<div class="container">
		
		<nav class="navbar">
			<a class="navbar-brand" href="<?php echo esc_url(home_url('/')) ?>">
				<img alt="" src="<?php the_field('header_logo', 'option') ?>">
			</a>
			
			<li class="nav-item header-phone-container" draggable="false">
				<a data-no-swup href="tel:<?php the_field('header_phone_number', 'option') ?>">
					<button class="auth-header-btn">
						<svg class="header-phone" height="20" viewBox="0 0 20 20" width="20">
							<path
								d="M3.96 2.037C8.341-1.3 14.62-.443 17.963 3.96c3.332 4.39 2.477 10.677-1.906 14.013C11.66 21.32 5.381 20.463 2.05 16.074-1.292 11.672-.437 5.384 3.96 2.037zm7.15 12.838c.971.405 2.748.009 2.996-.513.193-.375.313-.654.381-.893.065-.215.13-.348-.05-.503l-1.739-1.442c-.255-.2-.45-.073-.673.097-.201.173-.717.67-.98.788-.113.044-.213-.005-.312-.055-.425-.258-1.08-.903-1.654-1.631-.558-.761-1.003-1.566-1.138-2.046-.02-.109-.041-.218.031-.315.184-.223.81-.575 1.02-.735.224-.17.398-.324.274-.624l-.923-2.064c-.1-.214-.246-.187-.47-.182-.249.001-.55.043-.962.129-.568.1-1.424 1.708-1.295 2.753.195 1.516.863 3.025 1.83 4.326.984 1.269 2.258 2.317 3.664 2.91z"
								fill="url(#header-phone-gradient)"/>
						</svg>
						<span><?php the_field('header_phone_number', 'option') ?></span>
					</button>
				</a>
			</li>
			
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav">
					<?php
					if (have_rows('header_menu', 'option')) : ?>
						<?php while (have_rows('header_menu', 'option')) : the_row() ?>
							<?php if (get_row_index() <= 3) : ?>
								<li class="nav-item" draggable="false">
									<a class="nav-link" draggable="false" href="<?php the_sub_field('header_menu_item_link') ?>">
										<button class="auth-header-btn"><?php the_sub_field('header_menu_item_name') ?></button>
									</a>
								</li>
							<?php else: ?>
								<li class="nav-item d-lg-none" draggable="false">
									<a class="nav-link" draggable="false" href="<?php the_sub_field('header_menu_item_link') ?>">
										<button class="auth-header-btn"><?php the_sub_field('header_menu_item_name') ?></button>
									</a>
								</li>
							<?php endif; ?>
						<?php endwhile; ?>
					<?php endif; ?>
				
				</ul>
			</div>
			<div class="auth-drop-down-wrapper">
				<button class="navbar-toggler auth-header-btn">
					<span></span><span></span><span></span>
				</button>
				<div class="auth-drop-down">
					<?php
					if (have_rows('header_menu', 'option')) : ?>
						<?php while (have_rows('header_menu', 'option')) : the_row() ?>
							<?php if (get_row_index() <= 3) : continue ?>
							
							<?php else: ?>
								<a class="item" href="<?php the_sub_field('header_menu_item_link') ?>"><?php the_sub_field('header_menu_item_name') ?></a>
							<?php endif; ?>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="navbar-collapse-wrapper d-lg-none">
				<div class="navbar-collapse-toggler">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
			
			<button class="auth-primary-btn open-contact-modal"><?php the_field('header_button_text', 'option') ?></button>
		
		</nav>
	</div>
	<div class="make-btn">
		<a href="<?php echo esc_url(home_url('/place-order/?target=payment#payment-form-wrap')) ?>">Make A Payment</a>
	</div>
</header>

<div class="main-header">
    <div class="row">
        <div class="header">
            <div class="logo">
                <img src="<?php the_field('header_logo','option') ?>" alt="">
            </div>
            <div class="email">
                <div class="avail" style="background-color: <?php the_field('primary_color','option') ?>">
                    <p><?php the_field('top_phone_number_text','option') ?></p>
                </div>
                <div style="background-color: <?php the_field('second__color_1','option') ?>" class="content">
                    <div class="phone">
                        <i class="fas fa-phone"></i>
                    </div>
                    <div class="number">
                        <p class="num-pho"><?php the_field('header_phone_number','option') ?></p>
                        <p class="help-desk"><?php the_field('bottom_phone_number_text','option') ?></p>
                    </div>
                </div>

                <div style="background-color: <?php the_field('second__color_2','option') ?>" class="mail">
                    <i class="fas fa-envelope"></i>
                    <p><a href="mailto:<?php the_field('header_email','option') ?>"><?php the_field('header_email','option') ?></a></p>
                </div>

            </div>

        </div>
    </div>
</div>
<div class="nav-bar">
    <div class="row">
        <nav class="nav">
            <ul class="nav-links" style="background-color: <?php the_field('primary_color','option') ?>">



	            <?php
	            if (have_rows('header_menu', 'option')) : ?>
		            <?php while (have_rows('header_menu', 'option')) : the_row() ;
                        $page_id = url_to_postid(get_sub_field('header_menu_item_link'));
                        ?>
                        <li class="list <?php if (get_queried_object_id() == $page_id){echo 'active';} ?>">
                        <a class="links" href="<?php the_sub_field('header_menu_item_link') ?>"><?php the_sub_field('header_menu_item_name') ?> </a>

                        <?php if (have_rows('header_sub_menu_items','option')) : ?>
                            <ul class="add-ul">
                                <?php while (have_rows('header_sub_menu_items','option')) : the_row() ?>
                                    <li><a href="<?php the_sub_field('header_sub_menu_item_link') ?>"><?php the_sub_field('header_sub_menu_item_name') ?></a></li>
                                <?php endwhile; ?>
                            </ul>
                        <?php endif; ?>
                    </li>
		            <?php endwhile; ?>
	            <?php endif; ?>


                <li class="icon">
                    <a href="#">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
                <ol>
	                <?php
	                if (have_rows('header_menu', 'option')) : ?>
		                <?php while (have_rows('header_menu', 'option')) : the_row() ?>
                            <li class="list <?php if (get_row_index() == 1){echo 'active';} ?>">
                                <a class="links" href="<?php the_sub_field('header_menu_item_link') ?>"><?php the_sub_field('header_menu_item_name') ?> </a>

				                <?php if (have_rows('header_sub_menu_items','option')) : ?>
                                    <ul class="add-ul">
						                <?php while (have_rows('header_sub_menu_items','option')) : the_row() ?>
                                            <li><a href="<?php the_sub_field('header_sub_menu_item_link') ?>"><?php the_sub_field('header_sub_menu_item_name') ?></a></li>
						                <?php endwhile; ?>
                                    </ul>
				                <?php endif; ?>
                            </li>
		                <?php endwhile; ?>
	                <?php endif; ?>
                </ol>
            </ul>
        </nav>
    </div>
    <div class="make-btn make-btn-popup">
        <a href="<?php echo esc_url(home_url('/place-order/?target=payment#payment-form-wrap')) ?>">Make A Payment</a>
    </div>
</div>

<style>
	.place-order-new-step-wrapper {
		align-items: center;
	}
	
	.place-order-new-step-wrapper .right-col {
		display: flex;
		flex-direction: column;
		align-items: flex-start;
	}
	
	.place-order-new-step-wrapper .right-col img {
		width: 109px;
		height: 109px;
	}
	
	.place-order-new-step-wrapper .right-col h2.text {
		margin: 0;
	}
	
	.place-order-new-step-wrapper .right-col .description {
		margin-top: 10px;
		color: #ffffff;
		font-family: Montserrat, sans-serif !important;
		font-size: 18px !important;
		font-weight: 300 !important;
		margin-left: 0 !important;
	}
	
	.place-order-new-step-wrapper .right-col button {
		margin-top: 22px;
		min-width: 200px;
		background-color: #ffffff;
		color: #01cc99;
		font-family: Montserrat, sans-serif;
		font-size: 18px;
		font-weight: 700;
		text-transform: uppercase;
	}
	
	.place-order-new-step-wrapper .right-col button i {
		color: #01cc99;
		font-size: 18px;
		margin-left: 10px;
	}
	
	.payment-option {
		display: flex;
		flex-direction: column;
		background: #fff;
		padding: 20px;
		border-radius: 10px;
		height: 100%;
		justify-content: center;
		align-items: center;
		text-align: center;
	}
	
	.payment-option img {
		width: 56px;
		height: 56px;
		margin-bottom: 15px;
	}
	
	.payment-option .the-title {
		color: #076881;
		font-family: Montserrat, sans-serif !important;
		font-size: 24px;
		font-weight: 700;
		text-transform: uppercase;
	}
	
	.payment-option .the-desc {
		color: #414042;
		font-family: Montserrat, sans-serif !important;
		font-size: 18px;
		font-weight: 300;
		margin-top: 15px;
	}
	
	.payment-option button {
		margin-top: 20px;
		padding: 12px 20px;
	}
	
	.payment-option button i {
		margin-left: 10px;
	}
	
	.or-wrapper {
		display: flex;
		align-items: center;
		justify-content: center;
		height: 100%;
		font-size: 40px;
		text-transform: uppercase;
	}

</style>