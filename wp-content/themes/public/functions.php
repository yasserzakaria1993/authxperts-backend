<?php
####################################################
# Include Files
####################################################
require_once( get_template_directory() . '/inc/helper-functions.php' );
require_once( get_template_directory() . "/inc/orders-cpt.php" );
require_once( get_template_directory() . "/inc/services-cpt.php" );
require_once( get_template_directory() . "/sdk/ajax-payment.php" );
require_once( get_template_directory() . "/inc/admin-order-table.php" );




####################################################
# Styles
####################################################
add_action('wp_enqueue_scripts', function () {
	//css
	wp_enqueue_style('main-css',get_template_directory_uri() . '/assets/css/main.css');
	wp_enqueue_style('nav-css',get_template_directory_uri() . '/assets/css/nav-authxperts.css');

	//js
	wp_enqueue_script( 'bundle-js', get_template_directory_uri() . '/assets/js/bundle.js',[],'1.0',true );
	wp_enqueue_script( 'main-js', get_template_directory_uri() . '/assets/js/main.js', ['jquery'],'1.0',true );
	wp_enqueue_script( 'nav-js', get_template_directory_uri() . '/assets/js/nav-authxperts.js',[],'1.0',true );
	wp_localize_script('main-js','os',[
		'ajax_url'  => admin_url('admin-ajax.php')
	]);
});

####################################################
# Setup Theme
####################################################
add_action('after_setup_theme', function () {
	add_theme_support('title-tag');
	add_theme_support('post-thumbnails');
});

####################################################
# ACF Options Page
####################################################
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> __('Theme General Settings'),
		'menu_title' 	=> __('Theme Options'),
		'menu_slug' 	=> 'theme-general-settings',
	));

}

show_admin_bar( false );


####################################################
# Blog Load More Posts AJAX
####################################################
add_action( 'wp_ajax_blog_load_more_posts', 'blog_load_more_posts' );
add_action( 'wp_ajax_nopriv_blog_load_more_posts', 'blog_load_more_posts' );
function blog_load_more_posts() {
	$page = (int)sanitize_text_field( $_POST['page'] );
	$cat = (int)sanitize_text_field( $_POST['cat'] );
	$posts = new WP_Query([
		'post_type' => 'post',
		'cat'       => $cat,
		'posts_per_page' => 4,
		'paged'     => $page
	]);
	if ($posts->have_posts()){
		while ($posts->have_posts()) : $posts->the_post(); ?>
			<div class="blog-sidebar-item-content iv-wp-from-bottom">
				<div class="article-date">
					<i class="fas fa-calendar-alt"></i>
					<?php the_time('d.m.Y') ?>
				</div>
				<h2 class="auth-main-super-title-typography">
					<?php the_title(); ?>
				</h2>
				<img alt="Article img" class="article-img" src="<?php the_post_thumbnail_url('large') ?>">

				<p class="auth-body-text-typography"><?php echo strip_tags( get_the_excerpt() ); ?></p>
				<a class="article-btn auth-primary-btn" href="<?php the_permalink() ?>">Read More</a>
			</div>
		<?php endwhile ;
	}else{
		echo "empty";
	}
	die();
}

####################################################
# Blog Posts Search AJAX
####################################################
add_action( 'wp_ajax_blog_posts_search', 'blog_posts_search' );
add_action( 'wp_ajax_nopriv_blog_posts_search', 'blog_posts_search' );

function blog_posts_search() {
	$val = sanitize_text_field( $_POST['val'] );
	$search = new WP_Query([
	   'post_type'      => 'post',
       'post_per_page'  => 10,
        's'             => $val
    ]);
	if ($search->have_posts()) {
	    while ($search->have_posts()) : $search->the_post(); ?>
            <div class="col-md-6 col-sm-12">
                <div class="search-post">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="post-img">
                                <a href="<?php the_permalink() ?>">
                                    <img src="<?php the_post_thumbnail_url('thumbnail') ?>" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="post-title">
                                <a href="<?php the_permalink() ?>">
                                    <h4><?php the_title() ?></h4>
                                </a>
                            </div>
                            <div class="post-excerpt">
                                <?php echo wp_trim_words(get_the_content(),25,'...')?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; wp_reset_query();
    }else{
	    echo "<div class='alert alert-info empty'>No There Any Search Result For This Keyword</div>";
    }
    die();
}

//add_filter('wpcf7_form_elements', function($content) {
//	$content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
//$content = str_replace('
//', ”, $content);
//
//return $content;
//});
add_filter('wpcf7_form_elements', function($content) {
	$content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

	return $content;
});


####################################################
# Place Order AJAX
####################################################
add_action( 'wp_ajax_place_order_country_data', 'place_order_country_data' );
add_action( 'wp_ajax_nopriv_place_order_country_data', 'place_order_country_data' );
function place_order_country_data() {
	$country = sanitize_text_field( $_POST['val'] );
    $data = '';
	if ( have_rows( 'countries', 'option' ) ) {
		while (have_rows('countries','option')) : the_row();
			if ( get_sub_field( 'country_name' ) == $country) {
				$data =  get_sub_field( 'country_information' );
			}else{
		        continue;
            }
        endwhile;
	}
	echo empty($data) ? 'empty' : $data ;
    die();
}


####################################################
# ACF JSON
####################################################
add_filter('acf/settings/save_json', 'my_acf_json_save_point');

function my_acf_json_save_point( $path ) {
	// update path
	$path = get_stylesheet_directory() . '/acf-json';
	// return
	return $path;
}
add_filter('acf/settings/load_json', 'my_acf_json_load_point');

function my_acf_json_load_point( $paths ) {
	// remove original path (optional)
	unset($paths[0]);
	// append path
	$paths[] = get_stylesheet_directory() . '/acf-json';
	// return
	return $paths;
}

####################################################
# Random String Func
####################################################
function generateRandomString($length = 10) {
	return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}


####################################################
# Mail Header
####################################################
function website_email() {
	$sender_email= get_option('admin_email');
	return $sender_email;
}
function website_name(){
	$site_name = get_option('blogname');
	return $site_name;
}
//add_filter('wp_mail_from','website_email');
//add_filter('wp_mail_from_name','website_name');


####################################################
# Contact Form 7
####################################################
add_action('wpcf7_before_send_mail', 'save_form' );

function save_form( $wpcf7 ) {

	$submission = WPCF7_Submission::get_instance();
    $form_title = $wpcf7->title();
    $post_data = $submission->get_posted_data();
	if ( $submission ) {
		if ( $form_title == 'placeorder 2 make payment' ) {

            $insert = wp_insert_post([
               'post_title' => 'Order From: ' . $post_data['fname'] . " " . $post_data['lname'] ,
               'post_status'    => 'publish',
                'post_type'     => 'orders'
            ]);

            if ( !is_wp_error( $insert) ){
	            //update_post_meta($insert,'reference_number', $post_data['reference_number']);
	            update_post_meta($insert,'order_quoted_amount', $post_data['payment-order']);
	            update_post_meta($insert,'donation_amount', $post_data['donation-amount']);
	            update_post_meta($insert,'full_name', $post_data['fname'] . " " . $post_data['lname']);
	            update_post_meta($insert,'business_name', $post_data['bname']);
	            update_post_meta($insert,'address', $post_data['address']);
	            update_post_meta($insert,'city', $post_data['city']);
	            update_post_meta($insert,'postal_code', $post_data['postal-code']);
	            update_post_meta($insert,'country', $post_data['country']);
	            update_post_meta($insert,'phone_number', $post_data['b-phone']);
	            update_post_meta($insert,'email', $post_data['b-email']);
	            if ( ! empty( $post_data['files'] ) ) {
		            //update_post_meta($insert,'files', implode( "\n", $post_data['files'] ) );
	            }
	            //update_post_meta($insert,'statuss', 'step_1');
	            $u_id = uniqid($insert . '-');
	            $password = generateRandomString();
	            update_post_meta($insert,'unique_id', $u_id);
	            update_post_meta($insert,'password', $password);

	            // attach to term
	            $term = explode( "@", $post_data['b-email'] );
	            wp_set_object_terms( $insert, $term[0], 'orders_cats' );
                // cf7 edit message
	            add_filter('wpcf7_display_message', function ($message, $status = '' ) use ($u_id, $password) {
		            //return   sprintf("<p class='order-link'>Your Order Link:<a href='%s' target='_blank'> %s </a></p><p class='order-password'>Your Password: %s</p>", home_url('/order/?id=' . $u_id),home_url('/order/?id=' . $u_id), $password);
		            return "Thanks For Submit, Your Order Link And Password Send To Your Email";
	            });

	            // send mail to user
                $subject = "Thanks For Make A Payment";
	            $to = sanitize_email( $post_data['b-email'] );
	            $msg = 'Thanks For Make A Payment';
	            $msg .= "\n";
	            $msg .= "Your Order Link: ";
	            $msg .= "\n";
                $msg .= home_url('/order/?id=' . $u_id);
	            $msg .= "\n";
	            $msg .= "Your Order Password: ";
	            $msg .= "\n";
	            $msg .= $password;
	            wp_mail( $to , $subject, $msg );

            }

		}
	}



}


####################################################
# Order Password
####################################################
add_action('wp_ajax_order_check_password','order_check_password');
add_action('wp_ajax_nopriv_order_check_password','order_check_password');
function order_check_password() {
	$org_id = sanitize_text_field($_POST['id']);
	$password = sanitize_text_field( $_POST['password'] );

	$pid = explode( '-', $org_id );
	$pid = (int)$pid[0];

	$name = get_post_meta( $pid, 'full_name', true );
	$email = get_post_meta( $pid, 'email', true );
	$order_password = get_post_meta( $pid, 'password', true );
	if ( $order_password == $password ) {
		$status = get_post_meta( $pid, 'status', true );
        ?>
        <div class="container">
            <div class="results">
                <div class="info">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="block">
                                <p><span>Name:</span> <?php echo $name ?></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="block">
                                <p><span>Email:</span> <?php echo $email ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="msg block">
                    <div class="alert">
                        <?php
                        $msg = '';
                        if (have_rows('statuss','option')){
	                        while (have_rows('statuss','option')) : the_row();
		                        $name = get_sub_field( 'status_name' );
		                        $value = strtolower( str_replace( ' ', '_', $name ) );
		                        $status_message = get_sub_field( 'status_message' );
		                        if ( $status == $value ) {
                                    $msg = $status_message;
		                        }else{
		                            continue;
                                }
	                        endwhile;
                        }
                        echo $msg;
                        ?>

                    </div>
                </div>
            </div>
        </div>
        <?php
	}else{
		echo "error";
    }
    die();
}

####################################################
# When Order Updated
####################################################
add_filter('acf/update_value/name=status', 'my_check_for_change', 10, 3);
function my_check_for_change($value, $post_id, $field) {
	$old_value = get_post_meta($post_id, 'statuss', true);
	$mail = get_post_meta($post_id, 'email', true);
	$status_select_obj = get_field_object( 'statuss', $post_id );
	$new_status = $status_select_obj['choices'][$field];

	if ($old_value != $value) {
		$status_msg = '';
		if (have_rows('statuss','option')){
			while (have_rows('statuss','option')) : the_row();
				$name = get_sub_field( 'status_name' );
				$value = strtolower( str_replace( ' ', '_', $name ) );
				$status_message = get_sub_field( 'status_message' );
				if ( $new_status == $value ) {
					continue ;
				}else{
					$status_msg = $status_message;
				}
			endwhile;
		}

		// it changed
		// send mail to user
		$subject = "Your Order Status Has Been Updated" ;
		$to = sanitize_email( $mail );
		$msg = 'Your Order Status Was Updated';
		$msg .= "\n";
		$msg .= "Message: " . $status_msg;
		wp_mail( $to, $subject, $msg );
	}
	return $value;
}


####################################################
# Home Location AJAX
####################################################
//add_action( 'wp_ajax_home_select_location', 'home_select_location' );
//add_action( 'wp_ajax_nopriv_home_select_location', 'home_select_location' );
function home_select_location() {
	$val = sanitize_text_field($_POST['val']);
	$pid = get_option('page_on_front');
	$text = 'os';

    if (have_rows('countries',$pid)) :  ?>
        <?php
        while (have_rows('countries',$pid)) : the_row() ;
	        $text = get_sub_field( 'country_text' );
	        if (get_sub_field('country_name') == $val) {
            }else{
                continue;
            }
        endwhile; ?>
    <?php endif;

    echo $text;
    die();
}


####################################################
# ACF Dynamic Select option
####################################################
add_filter('acf/load_field/key=field_5d091182de312', function( $field ){

	// options of the choices to add to select
	$field['choices'] = array();

	if (have_rows('statuss','option')){
		while (have_rows('statuss','option')) : the_row();
		    $name = get_sub_field( 'status_name' );
			$value = strtolower( str_replace( ' ', '_', get_sub_field( 'status_name' ) ) );
			$field['choices'][$value] = $name;
		endwhile;
    }

	// All done!
	return $field;

} );
remove_filter ('acf_the_content', 'wpautop');
