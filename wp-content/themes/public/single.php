<?php get_header(); ?>

<div class="single-blog-page-wrapper">
	<?php while (have_posts()) : the_post() ?>
		<div class="page-cover auth-filter-gradient-color">
		<img alt="Single Article Cover" class="image-cover no-zoom" src="<?php the_post_thumbnail_url('full') ?>">
		<div class="container">
			<a href="<?php echo esc_url(home_url('/blog')); ?>"><p class="auth-page-cover-subtitle iv-wp-from-top"><span class="square-cover"></span>Back to the blog</p></a>
			<h1 class="page-cover-main-title iv-wp-from-bottom"><?php the_title() ?></h1>
		</div>
	</div>

	<?php endwhile; ?>
	<div class="container" >
		<div class="blog-article-content iv-wp-from-bottom">
			<div class="article-date">
				<i class="fas fa-calendar-alt"></i>
				<?php the_date('d.m.Y') ?>
			</div>

			<?php the_content() ?>

            <?php if (get_field('show_social_share')) : ?>
                <div class="social-btns iv-wp-from-bottom">
                    <a class="btn facebook" href="#"><i class="fab fa-facebook hover-lighten-f"></i></a>
                    <a class="btn twitter" href="#"><i class="fab fa-twitter hover-lighten"></i></a>
                    <a class="btn google" href="#"><i class="fab fa-pinterest"></i></a>
                    <a class="btn skype" href="#"><i class="fab fa-linkedin"></i></a>
                </div>
            <?php endif; ?>
		</div>
	</div>
</div>

<?php get_footer() ?>
