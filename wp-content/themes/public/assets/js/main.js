(function ($) {

    // blog load more
    $('.blog-page-wrapper #load-more').on('click',function () {
        var _this = $(this);
        var page = $(_this).attr('data-page');
        var catID = $(_this).attr('data-cat');
        $.ajax({
            url: os.ajax_url,
            type: 'POST',
            data:{
                action: 'blog_load_more_posts',
                page: page,
                cat: catID
            },
            beforeSend:function () {
                $(_this).html('Load More <i class="loading-icon far fa-spinner fa-pulse"></i>');
                $(_this).attr('disabled', true);
            },
            success:function (data) {
                $(_this).html('Load More');
                if (data == 'empty'){
                    $(_this).fadeOut(500);
                } else {
                    $(_this).before(data);
                    $(_this).attr('data-page', $(_this).attr('data-page') + 1);
                    $(_this).attr('disabled', false);
                }

            }
        })

    });

    // blog posts search
    $('.blog-page-wrapper #posts-search input').on('keyup',function () {
        var _this = $(this);
        var val = _this.val();
        if ($(_this).val().length >= 3){
            $.ajax({
                url: os.ajax_url,
                type: 'POST',
                data:{
                    action: 'blog_posts_search',
                    val: val,
                },
                beforeSend:function () {
                    $('.blog-page-wrapper .result .row').html('');
                    $('.blog-page-wrapper .result').fadeIn(200);
                    $('.blog-page-wrapper .result .loader').fadeIn(0)
                },
                success:function (data) {
                    $('.blog-page-wrapper .result .loader').fadeOut(0);
                   $('.blog-page-wrapper .result .row').html(data);
                }
            })
        }


    });

    $('.blog-page-wrapper .container > .row').on('click', function () {
        $('.blog-page-wrapper .result').slideUp(300)
    });

    //$('form.wpcf7-form').unwrap();
    $(document).on('wpcf7submit', function () {
       // alert('mailsent.wpcf7 was triggered!');
    });

    $('#order .pass-form-wrap form').on('submit',function (e) {
        e.preventDefault();
        var btn = $(this).find('button[type="submit"]');
        var wrap = $('#order');
        var alert = $('#order .pass-form-wrap .alert');
        $.ajax({
            url: os.ajax_url,
            type: 'POST',
            processData: false,
            data: $(this).serialize(),
            beforeSend:function () {
                btn.attr('disabled', true);
                btn.text('Please Wait...');
            },
            success:function (data) {
                if (data == 'error') {
                    alert.slideDown(500);
                    btn.text('Submit');
                    btn.attr('disabled', false);
                }else {
                    wrap.html(data);
                }
            }
        })

    });

    //wpcf7.initForm( jQuery('.wpcf7-form') );

    $('.pop-left-open').on('click',function (e) {

        if ( $(this).hasClass('closed') ){
            $('.pop-left').slideDown(300);
            $(this).removeClass('closed').addClass('opened');
        }else {
            $('.pop-left').slideUp(300);
            $(this).removeClass('opened').addClass('closed');
        }


    });

})(jQuery);