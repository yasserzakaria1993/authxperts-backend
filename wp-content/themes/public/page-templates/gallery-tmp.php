<?php
// template name:Gallery
get_header();
?>

<div class="gallery-page-wrapper">
    <div class="page-cover auth-filter-gradient-color">
        <img alt="Services Cover" class="image-cover no-zoom" src="<?php the_field('cover_image') ?>">
        <div class="container">
            <p class="auth-page-cover-subtitle iv-wp-from-left"><span class="square-cover"></span><?php the_field('cover_top_word') ?></p>
            <h1 class="page-cover-main-title iv-wp-from-top"><?php the_field('cover_main_text') ?></h1>
        </div>
    </div>

    <section class="gallery-details-container">
        <div class="container">
            <?php if (have_rows('blocks')) : ?>
                <?php while (have_rows('blocks')) : the_row() ?>
                    <div class="details iv-wp-from-bottom">
                        <h3 class="title iv-wp-from-left auth-secondary-color"><?php the_sub_field('block_title') ?></h3>
                        <h4 class="sub-title iv-wp-from-left auth-primary-color">
                            <i class="fas fa-calendar auth-primary-color"></i>
	                        <?php the_sub_field('block_date') ?>
                        </h4>
                        <div class="category-images row justify-content-start">
                            <?php while (have_rows('block_images')) : the_row() ?>
                                <div class="col-12 col-sm-6 col-lg-4 d-flex justify-content-center">
                                    <div class="image-block">
                                        <div class="image-frame iv-wp-from-top auth-primary-border-color">
                                            <a data-fancybox="gallery" data-caption="US Office" href="<?php the_sub_field('image') ?>" class="gallery-item">
                                                <img src="<?php the_sub_field('image') ?>" alt="">
                                            </a>
                                        </div>
                                        <div class="description auth-tertiary-color iv-wp-from-bottom"><?php the_sub_field('image_name') ?></div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>


        </div>

    </section>

</div>

<?php get_footer(); ?>
