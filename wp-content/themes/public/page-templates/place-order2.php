<?php
// template name: Place-order 2
get_header();
?>
<div class="place-order-page-wrapper">
	
	<div class="page-cover auth-filter-gradient-color">
		<img alt="Services Cover" class="image-cover no-zoom" src="<?php the_field('cover_image') ?>">
		<div class="container">
			<p class="auth-page-cover-subtitle iv-wp-from-left"><span class="square-cover"></span><?php the_field('cover_top_word') ?></p>
			<h1 class="page-cover-main-title iv-wp-from-top"><?php the_field('cover_main_text') ?></h1>
			<h3 class="page-cover-sub-title iv-wp-from-top"><?php the_field('cover_sub_main_text') ?></h3>
		</div>
	</div>
	
	
	<section class="place-order-details-container">
		<div class="container">
			<div class="text">
				<h3 class="text-center">It's Simple.</h3>
				<p class="text-center">
					Once you have sent us your documents, you can relax and let Authxperts take care of the authentication procedure. So call us today on 1-866-721-0746.
					To find out more about how we charge for our services please see the 'Fees' section below.
				</p>
			</div>
			<div class="place-order-steps-container auth-primary-color">
				<div class="content">
					<div class="row">
						<div class="col-lg-5 col-md-12 p-0 justify-content-center">
							<div class="get-quote">
								<div id="step-1" class="step">
									<div class="row ali place-order-new-step-wrapper">
										<div class="col-md-2">
											<div class="number iv-wp-from-top">01</div>
										</div>
										<div class="col-md-9 offset-1">
											<div class="right-col">
												<img class="no-zoom" src="<?php the_image_src('debt.png') ?>" alt="">
												<h2 class="text auth-neutral-color iv-wp-from-right">Get a quote</h2>
												<div class="description auth-neutral-color iv-wp">
													Contact Authxperts and get a quote for all of your documents.
												</div>
												<button class="auth-neutral-color iv-wp-from-bottom">GET A QUOTE <i class="fal fa-chevron-right"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-5 col-md-12 p-0 justify-content-center">
							<div class="option">
								<div id="step-2" class="step">
									<div class="row ali place-order-new-step-wrapper">
										<div class="col-md-2">
											<div class="number iv-wp-from-top">02</div>
										</div>
										<div class="col-md-9 offset-1">
											<div class="right-col">
												<img class="no-zoom" src="<?php the_image_src('documents.png') ?>" alt="">
												<h2 class="text auth-neutral-color iv-wp-from-right">Make a payment</h2>
												<div class="description auth-neutral-color iv-wp">
													choose to pay online or fill the data and we will make the payment for you
												</div>
												<button class="step-action-btn auth-neutral-color iv-wp-from-bottom">Make a payment <i class="fal fa-chevron-right"></i></button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-2 col-md-12 p-0 justify-content-center">
							<div class="option">
								<div id="step-3" class="step">
									<div class="place-order-new-step-wrapper">
										<div class="right-col">
											<div class="number iv-wp-from-top">03</div>
											<h2 class="text auth-neutral-color iv-wp-from-right">Relax</h2>
											<div class="description auth-neutral-color iv-wp">
												let Authxperts take care of everything.
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="options" style="<?php if (@$_GET['target'] == 'payment') {
						echo 'display:block !important';
					} ?>">
						<div class="row">
							<div class="col-lg-4 offset-lg-2">
								<div class="payment-option payment">
									<img class="no-zoom" src="<?php the_image_src('debit-card.png') ?>" alt="">
									<h3 class="the-title">Make Payment</h3>
									<p class="the-desc">Make your secure payment to our accounts by many avilable methods.</p>
									<button class="auth-primary-btn iv-wp-from-bottom">Make secure payment <i class="fal fa-chevron-right"></i></button>
								</div>
							</div>
							<div class="col-lg-1">
								<div class="or-wrapper">
									<h3 class="or-text">Or</h3>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="payment-option document">
									<img class="no-zoom" src="<?php the_image_src('bill.png') ?>" alt="">
									<h3 class="the-title">Post Documents</h3>
									<p class="the-desc">Send us your documents through our Authxperts transmittal form</p>
									<button class="auth-primary-btn iv-wp-from-bottom">Post your Documents <i class="fal fa-chevron-right"></i></button>
								</div>
							</div>
						
						
						</div>
						<div id="payment-form-wrap" class="payment-content" style="<?php if (@$_GET['target'] == 'payment') {
							echo 'display:block';
						} ?>">
							<?php echo str_replace('”', '', do_shortcode('[contact-form-7 id="475" title="placeorder 2 make payment" html_id="make-payment"]')) ?>
						</div>
						<div class="document-content">
							<?php //echo do_shortcode('[contact-form-7 id="453" title="Post Document For placeorder 2"]') ?>
							<div class="doc-link">
								<div class="text">
									<p>Please use the link below to download the Authentication Transmittal Form. You will then need to fill in all the details, print the filled form, sign it and enclose it with your documents. (The form should open in a pdf viewer such as Adobe Acrobat. If
										you do not have a pdf viewer, click <a href="https://get.adobe.com/reader/" target="_blank">here</a> to download one).</p>
									<p>Please state clearly the name of the country in which you intend to use the documents.</p>
								</div>
								<div class="link">
									<a download href="http://authxpertsllc.wpengine.com/wp-content/uploads/2019/07/authxperts_form-4.pdf">
										<i class="fa fa-link"></i>
										<span>Authentication Transmittal form</span>
									</a>
								</div>
							
							</div>
						</div>
					</div>
				
				</div>
			
			</div>
		</div>
	</section>
	
	<section class="estimate-price iv-wp-from-bottom">
		<div class="container">
			<div class="row justify-content-center">
				<h2 class="title auth-secondary-color col-12 iv-wp-from-top"><?php the_field('bottom_block_title') ?></h2>
				<div class="col-12"></div>
				<p class="description auth-tertiary-color col-12 col-lg-8 iv-wp"><?php the_field('bottom_block_text') ?></p>
				<img src="<?php the_image_src('X_shape.png') ?>" alt="" class="x-bg no-zoom iv-wp-from-bottom">
			</div>
			
			<div class="row justify-content-aroundxx">
				<div class="input-group iv-wp-from-bottom col-lg-4 col-md-5 col-sm-6 col-12">
					<label for="service">Select the type of service</label>
					<select name="service" id="service">
						<option>SELECT TYPE OF SERVICE</option>
						<option value="legalization">EGALIZATION</option>
						<option value="translation">TRANSLATION</option>
						<option value="visa service">VISA SERVICE</option>
						<option value="other">OTHER</option>
					</select>
				</div>
				<div style="display: none" class="input-group select2-box-wrap iv-wp-from-bottom col-lg-4 col-md-5 col-sm-6 col-12">
					<label for="origin-country">Select country of origin of document</label>
					<select name="origin-country" id="origin-country">
						<option>SELECT A COUNTRY</option>
						<option value="usa">USA</option>
						<option value="uk">UK</option>
						<option value="canada">CANADA</option>
						<option value="australia">AUSTRALIA</option>
						<option value="new zealand">NEW ZEALAND</option>
						<option value="other">OTHER</option>
					</select>
				</div>
				<div style="display: none" class="input-group select3-box-wrap iv-wp-from-bottom col-lg-4 col-md-5 col-sm-6 col-12">
					<label for="to-country">Select country where document will be used</label>
					<select name="to-country" id="to-country">
						<option value="none">SELECT TYPE OF SERVICE</option>
						
						<option value="uae">UAE</option>
						<option value="qatar">QATAR</option>
						<option value="kuwait">KUWAIT</option>
						<option value="egypt">EGYPT</option>
						<option value="bahrain">BAHRAIN</option>
						<option value="saudi arabia">SAUDI ARABIA</option>
						<option value="south korea">SOUTH KOREA</option>
						<option value="other">OTHER</option>
					</select>
				</div>
				
				<div class="col-12">
					<div class="estimated iv-wp-from-bottom" style="display: none">
						<p class="text auth-neutral-color iv-wp-from-left">Estimated fees for single document is</p>
						<div class="number iv-wp-from-right">$250</div>
					</div>
				</div>
				<div class="col-12 col-md-12">
					<div class="offer auth-tertiary-color iv-wp">
						We offer discounts for multiple documents. We have a Rate-match policy. Contact us for more details.
					</div>
				</div>
			</div>
		</div>
	
	
	</section>


</div>

<?php get_footer(); ?>

