<?php
// template name:Services_2
get_header();
?>

<div class="services_2-page-wrapper">
	<div class="page-cover auth-filter-gradient-color">
		<img alt="Services Cover" class="image-cover no-zoom" src="<?php the_field('cover_image') ?>">
		<div class="container">
			<p class="auth-page-cover-subtitle iv-wp-from-left"><span class="square-cover"></span><?php the_field('cover_top_word') ?></p>
			<h1 class="page-cover-main-title iv-wp-from-top"><?php the_field('cover_main_text') ?></h1>
		</div>
	</div>
	<div class="services_wrapper">
		<div class="container iv-wp-from-bottom">
			<div class="row">

                <?php
                $services = new WP_Query([
                   'post_type'  => 'our-services',
                   'post_per_page'  => -1,
                ]);
                while ($services->have_posts()) : $services->the_post(); ?>
				    <div class="col-lg-4 col-md-6 col-12">
					    <a href="<?php the_permalink() ?>">
						<div class="service-item-wrapper">
							<div class="row">
								<div class="col-4">
									<img src="<?php the_field('cover_image') ?>" alt="">
								</div>
								<div class="col-8">
									<h3 class="service_title"><?php the_title(); ?></h3>
									<p class="service_decs"><?php the_field('short_description') ?></p>
								</div>
							</div>
						</div>
					</a>
				    </div>
                <?php endwhile; wp_reset_query(); ?>

			</div>
		</div>
		<div class="services-content">
			<div class="container">
				
				<?php if (have_rows('tabs')) : $a_num = 1; ?>
					<?php while (have_rows('tabs')) : the_row() ?>
						<div class="service-item-content iv-wp-from-bottom <?php if ($a_num == 1) {
							echo 'active';
						} ?>" data-number="<?php echo $a_num; ?>">
							<?php if (have_rows('tab_content')) : $num = 1; ?>
								<?php while (have_rows('tab_content')) : the_row() ?>
									
									<?php if (get_row_layout() == 'countries_select')  : ?>
										<select class="countries iv-wp-from-bottom" id="" name="country">
											<?php if (have_rows('countries_block')) : $num = 1; ?>
												<?php while (have_rows('countries_block')) : the_row() ?>
													<option value="<?php the_sub_field('country_code') ?>"><?php the_sub_field('country_name') ?></option>
												<?php endwhile; ?>
											<?php endif; ?>
										</select>
									<?php endif; ?>
									
									<?php if (get_row_layout() == 'countries_list_with_flag')  : ?>
										<h2 class="service-main-title auth-secondary-color"><?php the_sub_field('block_title') ?></h2>
										<div class="countries-flags">
											<div class="row">
												<?php if (have_rows('countries_list')) : $num = 1; ?>
													<?php while (have_rows('countries_list')) : the_row() ?>
														<div class="col-xl-2 col-lg-3 col-sm-4 col-6">
															<div class="country-item iv-wp-from-bottom">
																<a href="<?php the_sub_field('link') ?>">
																	<img alt="Flag" src="<?php the_sub_field('country_flag') ?>">
																	<h4 class="country-title"><?php the_sub_field('country_name') ?></h4>
																</a>
															</div>
														</div>
													<?php endwhile; ?>
												<?php endif; ?>
											</div>
										</div>
									<?php endif; ?>
									
									<?php if (get_row_layout() == 'gallery')  : ?>
										<h2 class="service-main-title auth-secondary-color iv-wp-from-bottom"><?php the_sub_field('gallery_block_title') ?></h2>
										<div class="continent-section">
											<div class="container">
												<div class="row">
													<?php if (have_rows('images')) : $num = 1; ?>
														<?php while (have_rows('images')) : the_row() ?>
															<div class="col-lg-4 col-sm-6 col-12">
																<div class="continent-item iv-wp-from-left">
																	<a href="<?php the_sub_field('link') ?>">
																		<img alt="Continent Img" src="<?php the_sub_field('image') ?>">
																		<div class="continent-title"><?php the_sub_field('image_title') ?></div>
																	</a>
																</div>
															</div>
														<?php endwhile; ?>
													<?php endif; ?>
												</div>
											</div>
										</div>
									<?php endif; ?>
									
									<?php if (get_row_layout() == 'text_block')  : ?>
										<?php if (have_rows('content')) : $num = 1; ?>
											<?php while (have_rows('content')) : the_row() ?>
												<h4 class="auth-small-title-typography iv-wp"><?php the_sub_field('title') ?></h4>
												<p class="auth-body-text-typography iv-wp "><?php the_sub_field('text') ?></p>
												<?php if (!empty(get_sub_field('link'))) : ?>
													<a class="auth-link-typography-with-arrow iv-wp-from-bottom" href="<?php echo get_sub_field('link')['url'] ?>"><?php echo get_sub_field('link')['title'] ?></a>
												<?php endif; ?>
											<?php endwhile; ?>
										<?php endif; ?>
									<?php endif; ?>
								
								<?php endwhile; ?>
							<?php endif; ?>
						</div>
						<?php $a_num++; endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>
