<?php
// template name: Place-order
get_header();
?>
<div class="place-order2-page-wrapper">
	
	<div class="page-cover auth-filter-gradient-color">
		<img alt="Services Cover" class="image-cover no-zoom" src="<?php the_field('cover_image') ?>">
		<div class="container">
			<p class="auth-page-cover-subtitle iv-wp-from-left"><span class="square-cover"></span><?php the_field('cover_top_word') ?></p>
			<h1 class="page-cover-main-title iv-wp-from-top"><?php the_field('cover_main_text') ?></h1>
			<h3 class="page-cover-sub-title iv-wp-from-top"><?php the_field('cover_sub_main_text') ?></h3>
		</div>
	</div>
	
	
	<section class="place-order-details-container">
		<div class="container">
			<div class="place-order-steps-container auth-main-gradient-color auth-primary-color">
				<h3 class="title iv-wp lines-center auth-neutral-color"><?php the_field('top_text') ?></h3>
				<div class="steps row">
					<div class="col-12 col-xl-3 offset-xl-2">
						<div id="step-1" class="step <?php if (@$_GET['target'] == 'step_1') {
							echo 'active';
						} ?>">
							<div class="number iv-wp-from-top">01</div>
							<div class="title">
								<div class="icon">
									<img src="<?php the_image_src('place-order/step-1-icon.png') ?>" alt="">
									<div class="bottom locator"></div>
									<div class="top locator"></div>
								</div>
								<div class="h-line left iv-wp-from-right"></div>
								<h2 class="text auth-neutral-color iv-wp-from-right">Get a quote</h2>
								<div class="h-line right"></div>
							</div>
							<div class="description auth-neutral-color iv-wp">Contact Authxperts and get a quote for all of your documents.
							</div>
							<button class="step-action-btn auth-neutral-color auth-secondary-btn iv-wp-from-bottom">GET A QUOTE</button>
							
							<?php echo do_shortcode('[contact-form-7 id="285" title="Get a quote" html_class=""]') ?>
						</div>
						<div id="step-2" class="step <?php if (@$_GET['target'] == 'step_2') {
							echo 'active';
						} ?>">
							<div class="number iv-wp-from-top">02</div>
							<div class="title">
								<div class="icon">
									<img src="<?php the_image_src('place-order/step-2-icon.png') ?>" alt="">
									<div class="bottom locator"></div>
									<div class="top locator"></div>
								</div>
								<div class="h-line left iv-wp-from-right"></div>
								<h2 class="text auth-neutral-color iv-wp-from-right">Make payment</h2>
								<div class="h-line right"></div>
							</div>
							<div class="description auth-neutral-color iv-wp">Make your secure payment to our accounts by many avilable methods.
							</div>
							<button class="step-action-btn auth-neutral-color auth-secondary-btn iv-wp-from-bottom payment-btn">Make Secure Payment</button>
							<?php echo do_shortcode('[contact-form-7 id="305" title="Make payment" html_id="payment-form"]') ?>
						
						</div>
						<div class="step <?php if (@$_GET['target'] == 'step_3') {
							echo 'active';
						} ?>">
							<div class="number iv-wp-from-top">03</div>
							<div class="title">
								<div class="icon">
									<img src="<?php the_image_src('place-order/step-3-icon.png') ?>" alt="">
									<div class="bottom locator"></div>
									<div class="top locator"></div>
								</div>
								<div class="h-line left iv-wp-from-right"></div>
								<h2 class="text auth-neutral-color iv-wp-from-right">Post documents </h2>
								<div class="h-line right"></div>
							</div>
							<div class="description auth-neutral-color iv-wp">Send us your documents through our Authxperts transmittal form</div>
							<button class="step-action-btn auth-neutral-color auth-secondary-btn iv-wp-from-bottom">Post Your Documents</button>
							<?php echo do_shortcode('[contact-form-7 id="306" title="Post documents"]') ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="estimate-price iv-wp-from-bottom">
		<div class="container">
			<div class="row justify-content-center">
				<h2 class="title auth-secondary-color col-12 iv-wp-from-top"><?php the_field('bottom_block_title') ?></h2>
				<div class="col-12"></div>
				<p class="description auth-tertiary-color col-12 col-lg-8 iv-wp"><?php the_field('bottom_block_text') ?></p>
				<img src="<?php the_image_src('X_shape.png') ?>" alt="" class="x-bg no-zoom iv-wp-from-bottom">
			</div>
			
			<div class="row justify-content-aroundxx">
				<div class="input-group iv-wp-from-bottom col-lg-4 col-md-5 col-sm-6 col-12">
					<label for="service">Select the type of service</label>
					<select name="service" id="service">
						<option>SELECT TYPE OF SERVICE</option>
						<option value="legalization">EGALIZATION</option>
						<option value="translation">TRANSLATION</option>
						<option value="visa service">VISA SERVICE</option>
						<option value="other">OTHER</option>
					</select>
				</div>
				<div style="display: none" class="input-group select2-box-wrap iv-wp-from-bottom col-lg-4 col-md-5 col-sm-6 col-12">
					<label for="origin-country">Select country of origin of document</label>
					<select name="origin-country" id="origin-country">
						<option>SELECT A COUNTRY</option>
						<option value="usa">USA</option>
						<option value="uk">UK</option>
						<option value="canada">CANADA</option>
						<option value="australia">AUSTRALIA</option>
						<option value="new zealand">NEW ZEALAND</option>
						<option value="other">OTHER</option>
					</select>
				</div>
				<div style="display: none" class="input-group select3-box-wrap iv-wp-from-bottom col-lg-4 col-md-5 col-sm-6 col-12">
					<label for="to-country">Select country where document will be used</label>
					<select name="to-country" id="to-country">
						<option value="none">SELECT TYPE OF SERVICE</option>
						
						<option value="uae">UAE</option>
						<option value="qatar">QATAR</option>
						<option value="kuwait">KUWAIT</option>
						<option value="egypt">EGYPT</option>
						<option value="bahrain">BAHRAIN</option>
						<option value="saudi arabia">SAUDI ARABIA</option>
						<option value="south korea">SOUTH KOREA</option>
						<option value="other">OTHER</option>
					</select>
				</div>
				
				<div class="col-12">
					<div class="estimated iv-wp-from-bottom" style="display: none">
						<p class="text auth-neutral-color iv-wp-from-left">Estimated fees for single document is</p>
						<div class="number iv-wp-from-right">$250</div>
					</div>
				</div>
				<div class="col-12 col-md-12">
					<div class="offer auth-tertiary-color iv-wp">
						We offer discounts for multiple documents. We have a Rate-match policy. Contact us for more details.
					</div>
				</div>
			</div>
		</div>
	
	
	</section>
</div>

<?php get_footer(); ?>
