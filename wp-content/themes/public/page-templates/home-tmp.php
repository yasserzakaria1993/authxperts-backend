<?php
// template name:Home
get_header();
?>
<div class="This_is_test_div"></div>
<div class="home-page-wrapper">
	<?php if (have_rows('home_options')) : ?>
		<?php while (have_rows('home_options')) : the_row() ;?>

			<?php if (get_row_layout() == 'home_slider') : ?>
                <section class="home-hero">
                    <?php $count = 1; while (have_rows('home_slider')) : the_row() ?>
                        <img src="<?php the_sub_field('background_image') ?>" alt="" class="hero-bg <?php  if ($count == 1){echo 'active';}?>">
                    <?php $count++; endwhile; ?>

                    <div class="hero-filter auth-filter-gradient-color"></div>
                    <div class="container">
                        <div class="row locations-row">
                            <div class="col-12 col-md-7">
	                            <?php $count = 1; while (have_rows('home_slider')) : the_row() ?>
                                    <div class="location-content initializing <?php  if ($count == 1){echo 'active';}?>">
                                        <h2 class="location-title auth-neutral-color iv-wp"><?php the_sub_field('main_title') ?></h2>
                                        <div class="location-description iv-wp"><?php the_sub_field('sub_title') ?></div>
                                        <?php if (!empty(get_sub_field('button_text'))) : ?>
                                            <div class="link-wrap">
                                                <a href="<?php the_sub_field('button_link') ?>" class="auth-primary-btn location-read-more iv-wp-from-left"><?php the_sub_field('button_text') ?></a>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php $count++; endwhile; ?>
                            </div>
                            <div class="col-12 col-md-5">
                                <div class="locations-container iv-wp-from-right">
	                                <?php $count = 0; while (have_rows('home_slider')) :the_row() ?>
                                        <div class="location <?php  if ($count == 0){echo 'active';}?>" data-location-number="<?php echo $count;?>">
                                            <h4 class="location-name auth-neutral-color"><?php the_sub_field('location_title') ?></h4>
                                            <h5 class="location-contact auth-primary-color" data-country="<?php echo strtolower( get_sub_field( 'location_title' ) ); ?>">Contact</h5>
                                        </div>
                                    <?php $count++; endwhile; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
			<?php endif; ?>

			<?php if (get_row_layout() == 'home_services') : ?>
                <section class="our-services-container container right-container">
                    <div class="container">
                        <h2 class="our-services-title auth-main-title-typography iv-wp-from-left"><?php the_sub_field('home_service_title') ?></h2>
                    </div>
                    <div class="our-services">
                        <?php while (have_rows('services')) : the_row() ?>
                            <div class="service <?php if (get_row_index() == 1){echo 'auth-slick-active';} ?>">
                                <h3 class="title auth-secondary-color iv-wp-from-left"><?php the_sub_field('service_title') ?></h3>
                                <img src="<?php the_sub_field('service_image') ?>" alt="" class="icon iv-wp-from-right">
                                <div class="w-100"></div>
                                <p class="description auth-tertiary-color iv-wp"><?php the_sub_field('service_descriptions') ?></p>
                                <div class="read-more iv-wp-from-bottom auth-primary-color hover-arrow">
                                    MORE
                                    <i class="fal fa-angle-right auth-primary-color"></i>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>

                </section>
			<?php endif; ?>

			<?php if (get_row_layout() == 'home_places') : ?>
                <section class="all-places">
                    <div class="container">

                        <h2 class="title auth-secondary-color iv-wp-from-top"><?php the_sub_field('home_places_title') ?></h2>
                        <div class="row justify-content-center">
                            <form action="" class="col-12 col-lg-6 iv-wp-from-bottom">
                                <select name="country" id="" class="countries">
                                    <?php if (have_rows('countries')) :  ?>
                                        <?php while (have_rows('countries')) : the_row() ?>
                                            <option data-text="<?php echo htmlentities( get_sub_field( 'country_text' ) ); ?>" value="<?php the_sub_field('country_name') ?>"><?php the_sub_field('country_name') ?></option>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                </select>
                                <div class="content">
	                                <?php if (have_rows('countries')) :  ?>
		                                <?php while (have_rows('countries')) : the_row() ?>
                                            <?php if (get_row_index() == 1)  : ?>
                                                <div class="description auth-tertiary-color iv-wp"><?php echo get_sub_field('country_text',false,false); ?></div>
                                            <?php else: continue; endif; ?>
		                                <?php endwhile; ?>
	                                <?php endif; ?>

                                </div>
                                <div class="separator iv-wp-from-bottom"></div>
                                <button type="button" class="auth-primary-btn submit-country-btn iv-wp-from-top open-contact-modal">Contact Our Advisors</button>
                            </form>
                        </div>
                    </div>
                    <img src="<?php the_image_src('X_shape.png') ?>" alt="" class="x-bg no-zoom iv-wp-from-bottom">
                </section>
			<?php endif; ?>

            <?php if (get_row_layout() == 'home_steps') : ?>
                <section class="simple-steps-section iv-wp-from-bottom">
                    <div class="container auth-main-gradient-color">
                        <div class="title auth-neutral-color iv-wp-from-top">It’s very simple</div>
                        <div class="sub-title auth-neutral-color iv-wp-from-bottom">Just two steps and you are done</div>
                        <div class="place-order-steps-container auth-main-gradient-color auth-primary-color iv-wp-from-bottom">
                            <div class="steps row">
                                <div class="col-12">
                                    <div class="step">
                                        <div class="number iv-wp-from-top">01</div>
                                        <div class="title">
                                            <div class="icon">
                                                <img src="<?php the_sub_field('step_1_icon') ?>" alt="" class="no-zoom">
                                                <div class="bottom locator"></div>
                                                <div class="top locator"></div>
                                            </div>
                                            <div class="h-line left"></div>
                                            <a href="<?php echo home_url('/place-order/?target=step_1') ?>">
                                                <h2 class="text auth-neutral-color iv-wp-from-bottom"><?php the_sub_field('step_1_title') ?></h2>
                                            </a>
                                            <div class="h-line right"></div>
                                        </div>
                                        <div class="description auth-neutral-color iv-wp"><?php the_sub_field('step_1_text') ?></div>
                                    </div>
                                    <div class="step">
                                        <div class="number iv-wp-from-top">02</div>
                                        <div class="title">
                                            <div class="icon">
                                                <img src="<?php the_sub_field('step_2_icon') ?>" alt="" class="no-zoom">
                                                <div class="bottom locator"></div>
                                                <div class="top locator"></div>
                                            </div>
                                            <div class="h-line left"></div>
                                            <a href="<?php echo home_url('/place-order/?target=step_2') ?>">
                                                <h2 class="text auth-neutral-color iv-wp-from-bottom"><?php the_sub_field('step_2_title') ?></h2>
                                            </a>
                                            <div class="h-line right"></div>
                                        </div>
                                        <div class="description auth-neutral-color iv-wp"><?php the_sub_field('step_2_text') ?></div>
                                    </div>
                                    <div class="step">
                                        <div class="number iv-wp-from-top">03</div>
                                        <div class="title">
                                            <div class="icon">
                                                <img src="<?php the_sub_field('step_3_icon') ?>" alt="" class="no-zoom">
                                                <div class="bottom locator"></div>
                                                <div class="top locator"></div>
                                            </div>
                                            <div class="h-line left"></div>
                                            <a href="<?php echo home_url('/place-order/?target=step_3') ?>">
                                                <h2 class="text auth-neutral-color iv-wp-from-bottom"><?php the_sub_field('step_3_title') ?> </h2>
                                            </a>
                                            <div class="h-line right"></div>
                                        </div>
                                        <div class="description auth-neutral-color iv-wp"><?php the_sub_field('step_3_text
                                        ') ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button onclick="location.href = '<?php echo home_url('/contact-us') ?>'" class="auth-secondary-btn iv-wp-from-bottom">Get Your Quote</button>
                </section>
			<?php endif; ?>

			<?php if (get_row_layout() == 'home_trusted') : ?>
                <section class="trusted">
                    <div class="container">
                        <h2 class="main-title auth-secondary-color iv-wp-from-top"><?php the_sub_field('home_trusted_title') ?></h2>
                        <div class="reviews-container row">
							<?php while (have_rows('trusted_blocks')) : the_row(); ?>
                                <div class="review-outer col-12 col-md-6 col-lg-4 iv-wp-from-bottom">
                                    <div class="review">
                                        <h3 class="title auth-primary-color"><?php the_sub_field('trusted_block_title') ?></h3>
                                        <h4 class="sub-title auth-secondary-color"><?php the_sub_field('trusted_block_sub_title') ?></h4>
                                        <p class="description auth-tertiary-color"><?php the_sub_field('trusted_block_desc') ?></p>
                                    </div>
                                </div>
							<?php endwhile; ?>
                        </div>
                    </div>
                </section>
			<?php endif; ?>

	        <?php if (get_row_layout() == 'home_companies') : ?>
                <section class="companies">
                    <div class="container">
                        <div class="companies-slider iv-wp-from-top">

                            <?php while (have_rows('companies_images')) : the_row()?>
                                <div class="company">
                                    <img src="<?php the_sub_field('companies_image') ?>" alt="">
                                </div>
                            <?php endwhile; ?>

                        </div>
                        <button onclick="location.href = '<?php the_sub_field('companies_button_link') ?>'" class="auth-primary-btn iv-wp-from-bottom"><?php the_sub_field('companies_button_text') ?></button>
                    </div>
                </section>
            <?php endif; ?>

            <?php if (get_row_layout() == 'reviews') : ?>
                <div class="reviews">
                    <?php the_sub_field('reviews_code') ?>
                </div>
            <?php endif; ?>


		<?php endwhile; ?>
	<?php endif; ?>







</div>

<?php get_footer(); ?>
