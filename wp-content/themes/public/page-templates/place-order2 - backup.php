<?php
// template name: Place-order 2
get_header();
?>
<div class="place-order2-page-wrapper">

	<div class="page-cover auth-filter-gradient-color">
		<img alt="Services Cover" class="image-cover no-zoom" src="<?php the_field('cover_image') ?>">
		<div class="container">
			<p class="auth-page-cover-subtitle iv-wp-from-left"><span class="square-cover"></span><?php the_field('cover_top_word') ?></p>
			<h1 class="page-cover-main-title iv-wp-from-top"><?php the_field('cover_main_text') ?></h1>
			<h3 class="page-cover-sub-title iv-wp-from-top"><?php the_field('cover_sub_main_text') ?></h3>
		</div>
	</div>



    <section class="place-order-details-container">
		<div class="container">

            <div class="content">
                <div class="row">
                    <div class="col-md-6 d-flex justify-content-center">
                        <div class="circle bg-blue get-quote">
                            <h3>Get a Quote</h3>
                        </div>
                    </div>

                    <div class="col-md-6 d-flex justify-content-center">
                        <div class="circle bg-yal option">
                            <h3>Select Option</h3>
                        </div>
                    </div>



                </div>

                <div class="options">
                    <div class="row justify-content-center">
                        <div class="circle bg-yal option payment">
                            <h3>Make  Payment</h3>
                        </div>
                        <div class="circle bg-yal option document">
                            <h3>Post Document</h3>
                        </div>
                    </div>
                    <div class="payment-content">
                        <form id="make-payment" action="#">
                            <div class="step-1">
                                <p>Please enter the total amount you wish to pay for legalization services as per quote (including postage).</p>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="payment-order">Order (Quoted) Amount: $ </label>
                                        </div>
                                        <div class="col-md-9">
                                            <input required type="text" name="payment-order" value="" size="40" class="form-control" id="payment-order">
                                        </div>
                                    </div>
                                </div>

                                <p class="tit">Authxperts has partnered with Breast Care foundation and Care to raise funds for the cause. We value your generosity and will match up to $5 for each donation. Do you wish to contribute to Breast Care foundation or Care? </p>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="donation-amount">Donation Amount: </label>
                                        </div>
                                        <div class="col-md-9">
                                            <select required type="text" name="donation-amount" value="" size="40" class="form-control" id="donation-amount">
                                                <option value="0">-NONE-</option>
                                                <option value="5">$5</option>
                                                <option value="10">$10</option>
                                                <option value="15">$15</option>
                                                <option value="20">$20</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <p>Billing Information</p>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="fname">First Name</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input required type="text" name="fname" value="" size="40" class="form-control" id="fname">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="lname">Last Name</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input required type="text" name="lname" value="" size="40" class="form-control" id="lname">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="bname">Business Name</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input required type="text" name="bname" value="" size="40" class="form-control" id="bname">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="address">Address</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input required type="text" name="address" value="" size="40" class="form-control" id="address">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="address">City</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input required type="text" name="city" value="" size="40" class="form-control" id="city">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="postal-code">Postal Code</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input  type="text" name="postal-code" value="" size="40" class="form-control" id="postal-code">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="country">Country: </label>
                                        </div>
                                        <div class="col-md-9">
                                            <select required type="text" name="country" value="" size="40" class="form-control" id="country">
                                                <option value="">-NONE-</option>
                                                <option value="US">United States</option>
                                                <option value="CA">Canada</option>
                                                <option value="AF">Afghanistan</option>
                                                <option value="AL">Albania</option>
                                                <option value="DZ">Algeria</option>
                                                <option value="AS">American Samoa</option>
                                                <option value="AD">Andorra</option>
                                                <option value="AO">Angola</option>
                                                <option value="AI">Anguilla</option>
                                                <option value="AQ">Antarctica</option>
                                                <option value="AG">Antigua and Barbuda</option>
                                                <option value="AR">Argentina</option>
                                                <option value="AM">Armenia</option>
                                                <option value="AW">Aruba</option>
                                                <option value="AU">Australia</option>
                                                <option value="AT">Austria</option>
                                                <option value="AZ">Azerbaidjan</option>
                                                <option value="BS">Bahamas</option>
                                                <option value="BH">Bahrain</option>
                                                <option value="BD">Bangladesh</option>
                                                <option value="BB">Barbados</option>
                                                <option value="BY">Belarus</option>
                                                <option value="BE">Belgium</option>
                                                <option value="BZ">Belize</option>
                                                <option value="BJ">Benin</option>
                                                <option value="BM">Bermuda</option>
                                                <option value="BT">Bhutan</option>
                                                <option value="BO">Bolivia</option>
                                                <option value="BA">Bosnia-Herzegovina</option>
                                                <option value="BW">Botswana</option>
                                                <option value="BV">Bouvet Island</option>
                                                <option value="BR">Brazil</option>
                                                <option value="IO">British Indian Ocean Territory</option>
                                                <option value="BN">Brunei Darussalam</option>
                                                <option value="BG">Bulgaria</option>
                                                <option value="BF">Burkina Faso</option>
                                                <option value="BI">Burundi</option>
                                                <option value="KH">Cambodia</option>
                                                <option value="CM">Cameroon</option>
                                                <option value="CV">Cape Verde</option>
                                                <option value="KY">Cayman Islands</option>
                                                <option value="CF">Central African Republic</option>
                                                <option value="TD">Chad</option>
                                                <option value="CL">Chile</option>
                                                <option value="CN">China</option>
                                                <option value="CX">Christmas Island</option>
                                                <option value="CC">Cocos (Keeling) Islands</option>
                                                <option value="CO">Colombia</option>
                                                <option value="KM">Comoros</option>
                                                <option value="CG">Congo</option>
                                                <option value="CK">Cook Islands</option>
                                                <option value="CR">Costa Rica</option>
                                                <option value="HR">Croatia</option>
                                                <option value="CU">Cuba</option>
                                                <option value="CY">Cyprus</option>
                                                <option value="CZ">Czech Republic</option>
                                                <option value="DK">Denmark</option>
                                                <option value="DJ">Djibouti</option>
                                                <option value="DM">Dominica</option>
                                                <option value="DO">Dominican Republic</option>
                                                <option value="TP">East Timor</option>
                                                <option value="EC">Ecuador</option>
                                                <option value="EG">Egypt</option>
                                                <option value="SV">El Salvador</option>
                                                <option value="GQ">Equatorial Guinea</option>
                                                <option value="ER">Eritrea</option>
                                                <option value="EE">Estonia</option>
                                                <option value="ET">Ethiopia</option>
                                                <option value="FK">Falkland Islands</option>
                                                <option value="FO">Faroe Islands</option>
                                                <option value="FJ">Fiji</option>
                                                <option value="FI">Finland</option>
                                                <option value="CS">Former Czechoslovakia</option>
                                                <option value="SU">Former USSR</option>
                                                <option value="FR">France</option>
                                                <option value="FX">France (European Territory)</option>
                                                <option value="GF">French Guyana</option>
                                                <option value="TF">French Southern Territories</option>
                                                <option value="GA">Gabon</option>
                                                <option value="GM">Gambia</option>
                                                <option value="GE">Georgia</option>
                                                <option value="DE">Germany</option>
                                                <option value="GH">Ghana</option>
                                                <option value="GI">Gibraltar</option>
                                                <option value="GB">United Kingdom</option>
                                                <option value="GR">Greece</option>
                                                <option value="GL">Greenland</option>
                                                <option value="GD">Grenada</option>
                                                <option value="GP">Guadeloupe (French)</option>
                                                <option value="GU">Guam (USA)</option>
                                                <option value="GT">Guatemala</option>
                                                <option value="GN">Guinea</option>
                                                <option value="GW">Guinea Bissau</option>
                                                <option value="GY">Guyana</option>
                                                <option value="HT">Haiti</option>
                                                <option value="HM">Heard and McDonald Islands</option>
                                                <option value="HN">Honduras</option>
                                                <option value="HK">Hong Kong</option>
                                                <option value="HU">Hungary</option>
                                                <option value="IS">Iceland</option>
                                                <option value="IN">India</option>
                                                <option value="ID">Indonesia</option>
                                                <option value="IR">Iran</option>
                                                <option value="IQ">Iraq</option>
                                                <option value="IE">Ireland</option>
                                                <option value="IL">Israel</option>
                                                <option value="IT">Italy</option>
                                                <option value="CI">Ivory Coast (Cote D'</option>
                                                <option value="JM">Jamaica</option>
                                                <option value="JP">Japan</option>
                                                <option value="JO">Jordan</option>
                                                <option value="KZ">Kazakhstan</option>
                                                <option value="KE">Kenya</option>
                                                <option value="KI">Kiribati</option>
                                                <option value="KW">Kuwait</option>
                                                <option value="KG">Kyrgyzstan</option>
                                                <option value="LA">Laos</option>
                                                <option value="LV">Latvia</option>
                                                <option value="LB">Lebanon</option>
                                                <option value="LS">Lesotho</option>
                                                <option value="LR">Liberia</option>
                                                <option value="LY">Libya</option>
                                                <option value="LI">Liechtenstein</option>
                                                <option value="LT">Lithuania</option>
                                                <option value="LU">Luxembourg</option>
                                                <option value="MO">Macau</option>
                                                <option value="MK">Macedonia</option>
                                                <option value="MG">Madagascar</option>
                                                <option value="MW">Malawi</option>
                                                <option value="MY">Malaysia</option>
                                                <option value="MV">Maldives</option>
                                                <option value="ML">Mali</option>
                                                <option value="MT">Malta</option>
                                                <option value="MH">Marshall Islands</option>
                                                <option value="MQ">Martinique (French)</option>
                                                <option value="MR">Mauritania</option>
                                                <option value="MU">Mauritius</option>
                                                <option value="YT">Mayotte</option>
                                                <option value="MX">Mexico</option>
                                                <option value="FM">Micronesia</option>
                                                <option value="MD">Moldavia</option>
                                                <option value="MC">Monaco</option>
                                                <option value="MN">Mongolia</option>
                                                <option value="MS">Montserrat</option>
                                                <option value="MA">Morocco</option>
                                                <option value="MZ">Mozambique</option>
                                                <option value="MM">Myanmar</option>
                                                <option value="NA">Namibia</option>
                                                <option value="NR">Nauru</option>
                                                <option value="NP">Nepal</option>
                                                <option value="NL">Netherlands</option>
                                                <option value="AN">Netherlands Antilles</option>
                                                <option value="NT">Neutral Zone</option>
                                                <option value="NC">New Caledonia (French)</option>
                                                <option value="NZ">New Zealand</option>
                                                <option value="NI">Nicaragua</option>
                                                <option value="NE">Niger</option>
                                                <option value="NG">Nigeria</option>
                                                <option value="NU">Niue</option>
                                                <option value="NF">Norfolk Island</option>
                                                <option value="KP">North Korea</option>
                                                <option value="MP">Northern Mariana Islands</option>
                                                <option value="NO">Norway</option>
                                                <option value="OM">Oman</option>
                                                <option value="PK">Pakistan</option>
                                                <option value="PW">Palau</option>
                                                <option value="PA">Panama</option>
                                                <option value="PG">Papua New Guinea</option>
                                                <option value="PY">Paraguay</option>
                                                <option value="PE">Peru</option>
                                                <option value="PH">Philippines</option>
                                                <option value="PN">Pitcairn Island</option>
                                                <option value="PL">Poland</option>
                                                <option value="PF">Polynesia (French)</option>
                                                <option value="PT">Portugal</option>
                                                <option value="PR">Puerto Rico</option>
                                                <option value="QA">Qatar</option>
                                                <option value="RE">Reunion (French)</option>
                                                <option value="RO">Romania</option>
                                                <option value="RU">Russian Federation</option>
                                                <option value="RW">Rwanda</option>
                                                <option value="GS">S. Georgia &amp; S. Sandwich Isls.</option>
                                                <option value="SH">Saint Helena</option>
                                                <option value="KN">Saint Kitts &amp; Nevis Anguilla</option>
                                                <option value="LC">Saint Lucia</option>
                                                <option value="PM">Saint Pierre and Miquelon</option>
                                                <option value="ST">Saint Tome (Sao Tome) and Principe</option>
                                                <option value="VC">Saint Vincent &amp; Grenadines</option>
                                                <option value="WS">Samoa</option>
                                                <option value="SM">San Marino</option>
                                                <option value="SA">Saudi Arabia</option>
                                                <option value="SN">Senegal</option>
                                                <option value="SC">Seychelles</option>
                                                <option value="SL">Sierra Leone</option>
                                                <option value="SG">Singapore</option>
                                                <option value="SK">Slovak Republic</option>
                                                <option value="SI">Slovenia</option>
                                                <option value="SB">Solomon Islands</option>
                                                <option value="SO">Somalia</option>
                                                <option value="ZA">South Africa</option>
                                                <option value="KR">South Korea</option>
                                                <option value="ES">Spain</option>
                                                <option value="LK">Sri Lanka</option>
                                                <option value="SD">Sudan</option>
                                                <option value="SR">Suriname</option>
                                                <option value="SJ">Svalbard and Jan Mayen Islands</option>
                                                <option value="SZ">Swaziland</option>
                                                <option value="SE">Sweden</option>
                                                <option value="CH">Switzerland</option>
                                                <option value="SY">Syria</option>
                                                <option value="TJ">Tadjikistan</option>
                                                <option value="TW">Taiwan</option>
                                                <option value="TZ">Tanzania</option>
                                                <option value="TH">Thailand</option>
                                                <option value="TG">Togo</option>
                                                <option value="TK">Tokelau</option>
                                                <option value="TO">Tonga</option>
                                                <option value="TT">Trinidad and Tobago</option>
                                                <option value="TN">Tunisia</option>
                                                <option value="TR">Turkey</option>
                                                <option value="TM">Turkmenistan</option>
                                                <option value="TC">Turks and Caicos Islands</option>
                                                <option value="TV">Tuvalu</option>
                                                <option value="UG">Uganda</option>
                                                <option value="UA">Ukraine</option>
                                                <option value="AE">United Arab Emirates</option>
                                                <option value="UY">Uruguay</option>
                                                <option value="UM">USA Minor Outlying Islands</option>
                                                <option value="UZ">Uzbekistan</option>
                                                <option value="VU">Vanuatu</option>
                                                <option value="VA">Vatican City State</option>
                                                <option value="VE">Venezuela</option>
                                                <option value="VN">Vietnam</option>
                                                <option value="VG">Virgin Islands (British)</option>
                                                <option value="VI">Virgin Islands (USA)</option>
                                                <option value="WF">Wallis and Futuna Islands</option>
                                                <option value="EH">Western Sahara</option>
                                                <option value="YE">Yemen</option>
                                                <option value="YU">Yugoslavia</option>
                                                <option value="ZR">Zaire</option>
                                                <option value="ZM">Zambia</option>
                                                <option value="ZW">Zimbabwe</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="state">State: (US Only):  </label>
                                        </div>
                                        <div class="col-md-9">
                                            <select required type="text" name="state" value="" size="40" class="form-control" id="state">
                                                <option value="">-NONE-</option>
                                                <option value="AL">Alabama</option>
                                                <option value="AK">Alaska</option>
                                                <option value="AS">American Samoa</option>
                                                <option value="AZ">Arizona</option>
                                                <option value="AR">Arkansas</option>
                                                <option value="CA">California</option>
                                                <option value="CO">Colorado</option>
                                                <option value="CT">Connecticut</option>
                                                <option value="DE">Delaware</option>
                                                <option value="DC">District of Columbia</option>
                                                <option value="FM">Federated States of Micronesia</option>
                                                <option value="FL">Florida</option>
                                                <option value="GA">Georgia</option>
                                                <option value="GU">Guam</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="ID">Idaho</option>
                                                <option value="IL">Illinois</option>
                                                <option value="IN">Indiana</option>
                                                <option value="IA">Iowa</option>
                                                <option value="KS">Kansas</option>
                                                <option value="KY">Kentucky</option>
                                                <option value="LA">Louisiana</option>
                                                <option value="ME">Maine</option>
                                                <option value="MH">Marshall Islands</option>
                                                <option value="MD">Maryland</option>
                                                <option value="MA">Massachusetts</option>
                                                <option value="MI">Michigan</option>
                                                <option value="MN">Minnesota</option>
                                                <option value="MS">Mississippi</option>
                                                <option value="MO">Missouri</option>
                                                <option value="MT">Montana</option>
                                                <option value="NE">Nebraska</option>
                                                <option value="NV">Nevada</option>
                                                <option value="NH">New Hampshire</option>
                                                <option value="NJ">New Jersey</option>
                                                <option value="NM">New Mexico</option>
                                                <option value="NY">New York</option>
                                                <option value="NC">North Carolina</option>
                                                <option value="ND">North Dakota</option>
                                                <option value="MP">Northern Mariana Islands</option>
                                                <option value="OH">Ohio</option>
                                                <option value="OK">Oklahoma</option>
                                                <option value="OR">Oregon</option>
                                                <option value="PW">Palau</option>
                                                <option value="PA">Pennsylvania</option>
                                                <option value="PR">Puerto Rico</option>
                                                <option value="RI">Rhode Island</option>
                                                <option value="SC">South Carolina</option>
                                                <option value="SD">South Dakota</option>
                                                <option value="TN">Tennessee</option>
                                                <option value="TX">Texas</option>
                                                <option value="UT">Utah</option>
                                                <option value="VT">Vermont</option>
                                                <option value="VI">Virgin Islands</option>
                                                <option value="VA">Virginia</option>
                                                <option value="WA">Washington</option>
                                                <option value="WV">West Virginia</option>
                                                <option value="WI">Wisconsin</option>
                                                <option value="WY">Wyoming</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button type="button" class="auth-primary-btn submit-btn">Make secure payment</button>
                                </div>
                            </div>




                            <div class="step-2">
                                <p>Payment Information</p>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="total">Total $: </label>
                                        </div>
                                        <div class="col-md-9">
                                            <input required type="text" name="total" value="" size="40" class="form-control" id="total">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="csc">CSC: </label>
                                        </div>
                                        <div class="col-md-9">
                                            <input required type="text" name="csc" value="" size="40" class="form-control" id="csc">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="expiration">Expiration month:  </label>
                                        </div>
                                        <div class="col-md-9">
                                            <input required type="text" name="expiration" value="" size="40" class="form-control" id="expiration">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="expiration-y">Expiration month:  </label>
                                        </div>
                                        <div class="col-md-9">
                                            <input required type="text" name="expiration-y" value="" size="40" class="form-control" id="expiration-y">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="expiration-y">Expiration month:  </label>
                                        </div>
                                        <div class="col-md-9">
                                            <input required type="text" name="expiration-y" value="" size="40" class="form-control" id="expiration-y">
                                        </div>
                                    </div>
                                </div>

                                <p>Billing Information</p>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="fname">First Name</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input required type="text" name="fname" value="" size="40" class="form-control" id="fname">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="lname">Last Name</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input required type="text" name="lname" value="" size="40" class="form-control" id="lname">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="bname">Business Name</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input required type="text" name="bname" value="" size="40" class="form-control" id="bname">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="city">City</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input required type="text" name="city" value="" size="40" class="form-control" id="city">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="state">State</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input  type="text" name="state" value="" size="40" class="form-control" id="state">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="zip">Zip Code:</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input  type="text" name="zip" value="" size="40" class="form-control" id="zip">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="email">Email:</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input  type="email" name="phone" value="" size="40" class="form-control" id="email">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="phone">Phone Number:</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input  type="text" name="phone" value="" size="40" class="form-control" id="phone">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="auth-primary-btn submit-btn">Pay Now</button>
                                </div>

                            </div>




                        </form>
                    </div>
                    <div class="document-content">
                        <?php echo do_shortcode('[contact-form-7 id="453" title="Post Document For placeorder 2"]') ?>
                    </div>
                </div>

            </div>

		</div>
	</section>


</div>

<?php get_footer(); ?>
