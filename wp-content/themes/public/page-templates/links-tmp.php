<?php
// template name:Links
get_header();
?>

<div class="links-page-wrapper">
    <div class="page-cover auth-filter-gradient-color">
        <img alt="Services Cover" class="image-cover no-zoom" src="<?php the_field('cover_image') ?>">
        <div class="container">
            <p class="auth-page-cover-subtitle iv-wp-from-left"><span class="square-cover"></span><?php the_field('cover_top_word') ?></p>
            <h1 class="page-cover-main-title iv-wp-from-top"><?php the_field('cover_main_text') ?></h1>
            <h3 class="page-cover-sub-title iv-wp-from-top"><?php the_field('cover_sub_main_text') ?></h3>
        </div>
    </div>

    <div class="container">
        <div class="auth-search-form iv-wp-from-bottom">
            <input placeholder="Search for a link" type="text">
            <i class="search-icon fal fa-search"></i>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-12">
                <div class="auth-tabs-sidebar d-none d-lg-block iv-wp-from-left">
	                <?php if (have_rows('countries')) : $num=1; ?>
		                <?php while (have_rows('countries')) : the_row(); ?>
                            <div class="auth-sidebar-item <?php if ($num == 1){echo 'active';} ?>" data-number="<?php echo $num; ?>"><?php the_sub_field('country_name'); ?></div>
		                <?php $num++; endwhile; ?>
	                <?php endif; ?>
                </div>
                <div class="auth-tabs-sidebar-select-wrapper d-lg-none">
                    <select class="auth-tabs-sidebar-select-mobile">
                        <option>Select ... </option>
	                    <?php if (have_rows('countries')) : $num=1; ?>
		                    <?php while (have_rows('countries')) : the_row(); ?>
                                <option class="auth-sidebar-item <?php if ($num == 1){echo 'active';} ?>" value="<?php echo $num ; ?>"><?php the_sub_field('country_name'); ?></option>
                            <?php $num++; endwhile; ?>
	                    <?php endif; ?>
                    </select>
                </div>
            </div>

            <div class="col-lg-8 col-sm-12">
                <div class="links-sidebar-content">
	                <?php if (have_rows('countries')) : $a_num=1; ?>
		                <?php while (have_rows('countries')) : the_row(); ?>
                            <div class="auth-sidebar-content-wrapper <?php if ($a_num == 1){echo 'active-tab';} ?>" data-number="<?php echo $a_num ; ?>">
                                <div class="links-sidebar-item-content iv-wp-from-bottom">
                                    <h3 class="links-main-title"><?php the_sub_field('block_title') ?></h3>
                                    <p class="auth-body-text-typography"><?php the_sub_field('block_text') ?></p>
                                    <?php if (get_sub_field('block_button_text')) : ?>
                                        <a href="<?php the_sub_field('block_button_link') ?>" class="links-button auth-primary-btn"><?php the_sub_field('block_button_text') ?></a>
                                    <?php endif; ?>

	                                <?php if (have_rows('block_locations')) : $num=1; ?>
                                            <div class="links-container">
		                                        <?php while (have_rows('block_locations')) : the_row(); ?>
                                                    <a href="<?php the_sub_field('location_link') ?>" class="links-link"><?php the_sub_field('location_name') ?></a>
			                                    <?php $num++; endwhile; ?>
                                            </div>
	                                <?php endif; ?>
                                </div>
                            </div>
                        <?php $a_num++; endwhile; ?>
	                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>



<?php get_footer(); ?>
