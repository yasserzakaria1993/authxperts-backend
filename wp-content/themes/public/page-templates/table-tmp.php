<?php
get_header();



?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style>
    #table-wrap #table_filter{
        text-align: left;
        margin-bottom: 20px;
    }

    #table-wrap #table_filter input{
        border: 1px solid #ddd;
        margin-top: 10px;
        font-size: 15px;
        padding: 5px;
    }

    #table thead th{
        font-size: 13px;
        text-align: center;
    }

    #table tbody tr td{
        font-size: 13px;
        text-align: center;
    }
</style>

<div class="table-page-wrapper">
	<div class="page-cover auth-filter-gradient-color">
		<img alt="Services Cover" class="image-cover no-zoom" src="<?php the_field('cover_image') ?>">
		<div class="container">
			<p class="auth-page-cover-subtitle iv-wp-from-left"><span class="square-cover"></span><?php the_field('cover_top_word') ?></p>
			<h1 class="page-cover-main-title iv-wp-from-top"><?php the_field('cover_main_text') ?></h1>
		</div>
	</div>
	<div class="table_wrapper">
		<div class="container iv-wp-from-bottom">
		   <div id="table-wrap">

               <table id="table" class="table table-striped table-bordered" style="width:100%">
                   <thead>
                    <tr>
                       <th>Full Name</th>
                       <th>Quoted Amount</th>
                       <th>Donation Amount</th>
                       <th>Country</th>
                        <th>Postal Code</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                   </tr>
                   </thead>
                   <tbody>
                       <tr>
                           <td>Osama Abdo</td>
                           <td>100$</td>
                           <td>0$</td>
                           <td>Egypt</td>
                           <td>12345</td>
                           <td>osama@osama.com</td>
                           <td>01281840607</td>
                       </tr>
                       <tr>
                           <td>Osama Abdo</td>
                           <td>100$</td>
                           <td>0$</td>
                           <td>Egypt</td>
                           <td>12345</td>
                           <td>osama@osama.com</td>
                           <td>01281840607</td>
                       </tr>
                       <tr>
                           <td>Osama Abdo</td>
                           <td>100$</td>
                           <td>0$</td>
                           <td>Egypt</td>
                           <td>12345</td>
                           <td>osama@osama.com</td>
                           <td>01281840607</td>
                       </tr>
                       <tr>
                           <td>Osama Abdo</td>
                           <td>100$</td>
                           <td>0$</td>
                           <td>Egypt</td>
                           <td>12345</td>
                           <td>osama@osama.com</td>
                           <td>01281840607</td>
                       </tr>
                       <tr>
                           <td>Osama Abdo</td>
                           <td>100$</td>
                           <td>0$</td>
                           <td>Egypt</td>
                           <td>12345</td>
                           <td>osama@osama.com</td>
                           <td>01281840607</td>
                       </tr>
                       <tr>
                           <td>Osama Abdo</td>
                           <td>100$</td>
                           <td>0$</td>
                           <td>Egypt</td>
                           <td>12345</td>
                           <td>osama@osama.com</td>
                           <td>01281840607</td>
                       </tr>                       <tr>
                           <td>Osama Abdo</td>
                           <td>100$</td>
                           <td>0$</td>
                           <td>Egypt</td>
                           <td>12345</td>
                           <td>osama@osama.com</td>
                           <td>01281840607</td>
                       </tr>


                   </tbody>
               </table>

           </div>
		</div>
	</div>
</div>


<?php get_footer(); ?>


<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script>
    (function ($) {
        var editor; // use a global for the submit and return data rendering in the examples
        $('#table').DataTable({
            "lengthChange": false,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    })(jQuery);
</script>
