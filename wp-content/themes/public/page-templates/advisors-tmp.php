<?php
// template name:Advisors
get_header();
?>

<div class="advisers-page-wrapper">
    <div class="page-cover auth-filter-gradient-color">
        <img alt="Services Cover" class="image-cover no-zoom" src="<?php the_field('cover_image') ?>">
        <div class="container">
            <p class="auth-page-cover-subtitle iv-wp-from-left"><span class="square-cover"></span><?php the_field('cover_top_word') ?></p>
            <h1 class="page-cover-main-title iv-wp-from-top"><?php the_field('cover_main_text') ?></h1>
        </div>
    </div>
    <section class="advisers-details-container">
        <div class="container">
	        <?php if (have_rows('blocks')) : ?>
	            <?php while (have_rows('blocks')) : the_row() ?>
                    <div class="details iv-wp-from-bottom">
                    <div class="adviser">
                        <h2 class="name iv-wp-from-top auth-secondary-color"><?php the_sub_field('name') ?></h2>
                        <h3 class="title iv-wp-from-right auth-primary-color"><?php the_sub_field('job') ?></h3>
                        <div class="avatar iv-wp-from-left">
                            <img alt="" src="<?php the_sub_field('avatar') ?>">
                        </div>
                    </div>
                    <p class="advise iv-wp auth-tertiary-color"><?php the_sub_field('text') ?></p>
                    <?php if (!empty(get_sub_field('link'))) : ?>
                        <div class="know-more-wrapper">
                            <a href="<?php echo get_sub_field('link')['url']  ?>" class="know-more iv-wp-from-left hover-arrow auth-primary-color">
	                            <?php echo get_sub_field('link')['title']  ?>
                                <i class="fal fa-angle-right auth-primary-color"></i>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
                <?php endwhile; ?>
            <?php endif; ?>

        </div>

    </section>

</div>
<?php get_footer(); ?>
