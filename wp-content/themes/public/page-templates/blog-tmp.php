<?php
// template name:Blog
get_header();
?>

<div class="blog-page-wrapper">
    <div class="page-cover auth-filter-gradient-color">
        <img alt="Services Cover" class="image-cover no-zoom" src="<?php the_field('cover_image') ?>">
        <div class="container">
            <p class="auth-page-cover-subtitle iv-wp-from-left"><span class="square-cover"></span><?php the_field('cover_top_word') ?></p>
            <h1 class="page-cover-main-title iv-wp-from-top"><?php the_field('cover_main_text') ?></h1>
        </div>
    </div>

    <div class="container">
        <div class="auth-search-form iv-wp-from-top" id="posts-search">
            <div class="search-form" style="position: relative">
                <input placeholder="Search for the articles" type="text">
                <i class="search-icon fal fa-search"></i>
            </div>

            <div class="result">
                <div class="loader text-center">
                    <img src="<?php the_image_src('loading.gif') ?>" alt="">
                </div>
                <div class="row">

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-12">
                <div class="auth-tabs-sidebar d-none d-lg-block iv-wp-from-left">
                    <?php if ( ! empty( get_field( 'categories' ) ) ) : $num = 1 ;?>
                        <?php foreach (get_field( 'categories' ) as $cat) : ?>
                            <div class="auth-sidebar-item <?php if ($num == 1){echo 'active';} ?>" data-number="<?php echo $num;?>"><?php echo $cat->name ?></div>
                        <?php  $num++; endforeach; ?>
                    <?php endif; ?>

                </div>
                <div class="auth-tabs-sidebar-select-wrapper d-lg-none">
                    <select class="auth-tabs-sidebar-select-mobile">
                        <option>Select Category</option>
	                    <?php if ( ! empty( get_field( 'categories' ) ) ) : $num = 1 ;?>
		                    <?php foreach (get_field( 'categories' ) as $cat) : ?>
                                <option class="auth-sidebar-item <?php if ($num == 1){echo 'active';} ?>" value="<?php echo $num;?>"><?php echo $cat->name ?></option>

			                    <?php  $num++; endforeach; ?>
	                    <?php endif; ?>
                    </select>
                </div>
            </div>
            <div class="col-lg-8 col-sm-12">
                <div class="blog-sidebar-content iv-wp-from-right">

	                <?php if ( ! empty( get_field( 'categories' ) ) ) : $num = 1 ;?>
		                <?php foreach (get_field( 'categories' ) as $cat) : ?>
                            <div class="auth-sidebar-content-wrapper <?php if ($num == 1){echo 'active-tab';} ?>" data-number="<?php echo $num;?>">
                                <?php
                                $query = new WP_Query([
                                   'post_type'      => 'post',
                                   'cat'            => (int)$cat->term_id,
                                   'posts_per_page' => 4
                                ]);
                                while ($query->have_posts()) : $query->the_post();?>
                                    <div class="blog-sidebar-item-content iv-wp-from-bottom">
                                        <div class="article-date">
                                            <i class="fas fa-calendar-alt"></i>
                                            <?php the_time('d.m.Y') ?>
                                        </div>
                                        <h2 class="auth-main-super-title-typography"><?php the_title(); ?></h2>
                                        <img alt="Article img" class="article-img" src="<?php the_post_thumbnail_url('large') ?>">

                                        <p class="auth-body-text-typography"><?php echo strip_tags( get_the_excerpt() ); ?></p>
                                        <a class="article-btn auth-primary-btn" href="<?php the_permalink() ?>">Read More</a>
                                    </div>
                                <?php endwhile; wp_reset_query(); ?>
                                <button id="load-more" data-cat="<?php echo $cat->term_id; ?>" data-page="2" class="load-more-btn loading auth-primary-btn">Load More</button>
                            </div>
                        <?php  $num++; endforeach; ?>
	                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
