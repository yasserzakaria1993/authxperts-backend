<?php
// template name:FAQ
get_header();
?>

<div class="faqs-page-wrapper">

    <div class="page-cover auth-filter-gradient-color">
        <img alt="Services Cover" class="image-cover no-zoom" src="<?php the_field('cover_image') ?>">
        <div class="container">
            <p class="auth-page-cover-subtitle iv-wp-from-left"><span class="square-cover"></span><?php the_field('cover_top_word') ?></p>
            <h1 class="page-cover-main-title iv-wp-from-top"><?php the_field('cover_main_text') ?></h1>
            <h3 class="page-cover-sub-title iv-wp-from-bottom"><?php the_field('cover_sub_main_text') ?></h3>
        </div>
    </div>

    <div class="container">
        <div class="auth-search-form iv-wp-from-bottom">
            <input placeholder="Ask your questions..." type="text">
            <i class="search-icon fal fa-search"></i>
        </div>

        <div class="row">
            <div class="col-lg-4 col-sm-12">
                <div class="auth-tabs-sidebar d-none d-lg-block iv-wp-from-left">
                    <?php if (have_rows('faqs')) : $num=1; ?>
                        <?php while (have_rows('faqs')) : the_row(); ?>
                            <div class="auth-sidebar-item <?php if ($num == 1){echo 'active';} ?>" data-number="<?php echo $num; ?>"><?php the_sub_field('main_title') ?></div>
	                    <?php $num++; endwhile; ?>
                    <?php endif; ?>
                </div>
                <div class="auth-tabs-sidebar-select-wrapper d-lg-none">
                    <select class="auth-tabs-sidebar-select-mobile">
                        <option>Select Category</option>
	                    <?php if (have_rows('faqs')) : $num=1; ?>
		                    <?php while (have_rows('faqs')) : the_row(); ?>
                                <option class="auth-sidebar-item <?php if ($num == 1){echo 'active';} ?>" value="<?php echo $num; ?>"><?php the_sub_field('main_title') ?></option>
                            <?php $num++; endwhile; ?>
	                    <?php endif; ?>
                    </select>
                </div>
            </div>
            <div class="col-lg-8 col-sm-12">
                <div class="faqs-sidebar-content">
	                <?php if (have_rows('faqs')) : $num=1; ?>
		                <?php while (have_rows('faqs')) : the_row(); ?>
                            <div class="auth-sidebar-content-wrapper <?php if ($num == 1){echo 'active-tab';} ?>" data-number="<?php echo $num; ?>">
	                            <?php if (have_rows('blocks')) : $s_num=1; ?>
		                            <?php while (have_rows('blocks')) : the_row(); ?>
                                        <div class="faqs-sidebar-item-content iv-wp-from-bottom <?php if ($s_num == 1){echo 'toggled';} ?>">
                                            <div class="the-question-header toggleable-slide-up-header toggleable-slide-up-header-opened" <?php if (!empty(get_sub_field('block_title_background_color'))){ echo printf('style="background-color: %s"',get_sub_field('block_title_background_color'));} ?>><?php the_sub_field('block_title') ?></div>
                                            <div class="toggleable-slide-up-body" >
                                                <div class="the-question-body" <?php if (!empty(get_sub_field('block_description_background_color'))){ echo printf('style="background-color: %s"',get_sub_field('block_description_background_color'));} ?>>
                                                    <span class="a-character">A</span>
                                                    <div class="answer-content"><?php the_sub_field('block_description') ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php $s_num++; endwhile; ?>
	                            <?php endif; ?>
                            </div>
                        <?php $num++; endwhile; ?>
	                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
