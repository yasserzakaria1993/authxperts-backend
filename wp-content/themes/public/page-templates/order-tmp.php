<?php
ob_start();
// template name: Order
get_header();
$id = '';
$org_id = '';
if ( isset( $_GET['id'] ) ) {
	$id = sanitize_text_field( $_GET['id'] );
	$id = explode( '-', $id );
	$id = $id[0];
	$org_id = sanitize_text_field( $_GET['id'] );
}else{
//	wp_safe_redirect( home_url( '/' ) );
//	exit();
}

//$order = get_post( (int) $id );
$status = get_post_meta( $id, 'statuss', true );
$password = get_post_meta( $id, 'password', true );
?>
<div class="order-page-wrapper">
    <div class="page-cover auth-filter-gradient-color">
        <img alt="Single Article Cover" class="image-cover no-zoom" src="<?php the_field('cover_image') ?>">
        <div class="container">
            <p class="auth-page-cover-subtitle iv-wp-from-left"><span class="square-cover"></span><?php the_field('cover_top_word') ?></p>
            <h1 class="page-cover-main-title iv-wp-from-top"><?php the_field('cover_main_text') ?></h1>
        </div>
    </div>


    <div id="order">
        <div style="" class="container">
            <div  class="pass-form-wrap">
                <div class="alert">Password Not Correct</div>
                <form action="" class="pass-form">
                    <input required name="id" id="id" <?php if (!empty(@$_GET['id'])){printf("value='%s'", $org_id);} ?> type="text" placeholder="ID" class="form-control">
                    <input required name="password" type="password" placeholder="Password" class="form-control">
                    <input type="hidden" name="action" value="order_check_password">
                    <button type="submit" class="auth-primary-btn">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>



<?php get_footer();  ob_end_flush(); ?>