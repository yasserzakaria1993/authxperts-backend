<?php
// template name:Contact
get_header();
?>

<div class="contact-page-wrapper">
    <div class="page-cover auth-filter-gradient-color">
        <img alt="Single Article Cover" class="image-cover no-zoom" src="<?php the_field('cover_image') ?>">
        <div class="container">
            <p class="auth-page-cover-subtitle iv-wp-from-left"><span class="square-cover"></span><?php the_field('cover_top_word') ?></p>
            <h1 class="page-cover-main-title iv-wp-from-top"><?php the_field('cover_main_text') ?></h1>
        </div>
    </div>

    <div class="contact-content">
        <div class="container">
            <div class="blog-article-content iv-wp-from-bottom auth-main-gradient-color" style="padding: 70px 40px;">
                <div class="row">
                    <div class="col-md-6">
                        <form class="" action="">
		                    <?php if (have_rows('countries','option')) : ?>
                                <div class="title auth-neutral-color">Choose Location</div>
                                <div class="input-group">
                                    <label for="select3-country">Select one Country</label>
                                    <select name="select-country" id="select3-country" class="countries">
					                    <?php while (have_rows('countries','option')) : the_row(); ?>
                                            <option data-text='<?php the_sub_field('country_information') ?>' data-map='<?php the_sub_field('country_map_frame') ?>' value="<?php echo strtolower( get_sub_field( 'country_name' ) ); ?>"><?php the_sub_field('country_name') ?></option>
					                    <?php endwhile; ?>
                                    </select>

                                </div>
                                <div class="info">
				                    <?php while (have_rows('countries','option')) : the_row();
					                    if (get_row_index() == 1){
						                    the_sub_field( 'country_information' );
					                    }else{
						                    continue;
					                    }
				                    endwhile; ?>
                                </div>
		                    <?php endif; ?>
                        </form>
	                    <?php echo do_shortcode('[contact-form-7 id="324" title="Untitled"]') ?>
                    </div>
                    <div class="col-md-6">
                        <div id="map" style="height: 100%">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14324.392836787601!2d32.412746649999995!3d26.16093085!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sar!2seg!4v1560712251340!5m2!1sar!2seg" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<?php get_footer(); ?>
