<?php
// template name:Services_2
get_header();
?>

<div class="services_2-page-wrapper">
	<div class="page-cover auth-filter-gradient-color">
		<img alt="Services Cover" class="image-cover no-zoom" src="<?php the_field('cover_image') ?>">
		<div class="container">
			<p class="auth-page-cover-subtitle iv-wp-from-left"><span class="square-cover"></span><?php the_field('cover_top_word') ?></p>
			<h1 class="page-cover-main-title iv-wp-from-top"><?php the_field('cover_main_text') ?></h1>
		</div>
	</div>
	<div class="services_wrapper">
		<div class="container iv-wp-from-bottom">
			<div class="row justify-content-center">

                <?php
                $services = new WP_Query([
                   'post_type'  => 'our-services',
                    'posts_per_page'    => -1
                ]);
                while ($services->have_posts()) : $services->the_post(); ?>
                    <div class="col-md-4">
                        <div class="card">
                        <div class="card-head">
                            <div class="row align-items-center">
                                <div class="col-md-9">
                                    <a href="<?php the_permalink() ?>">
                                        <h2 class="card-title"><?php the_title() ?></h2>
                                    </a>
                                </div>
                                <div class="col-md-3 text-center">
                                    <?php the_field('service_icon'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="card-desc">
                            <p><?php echo get_post_meta(get_the_ID(),'short_description', true) ?></p>
                        </div>
                        <div class="card-btn">
                            <a href="<?php the_permalink() ?>" class="auth-primary-btn">Read More <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                    </div>
                <?php endwhile; ?>

			</div>

		</div>
        <div id="reviews">
            <?php the_field('reviews', get_queried_object_id()) ?>
        </div>
	</div>
</div>


<?php get_footer(); ?>
