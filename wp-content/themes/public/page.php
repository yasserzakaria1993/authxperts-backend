<?php get_header(); ?>


<div class="single-blog-page-wrapper">

	<div class="page-cover auth-filter-gradient-color">
		<img alt="Single Article Cover" class="image-cover no-zoom" src="<?php the_field('text_page_copy','option') ?>">
		<div class="container">
			<p class="auth-page-cover-subtitle iv-wp-from-left"><span class="square-cover"></span><?php the_title() ?></p>
		</div>
	</div>

	<div class="contact-content">
		<div class="container">
			<div class="blog-article-content iv-wp-from-bottom" style="padding: 70px 40px;">
                <?php while (have_posts()) : the_post(); ?>
				    <?php the_content() ?>
                <?php endwhile; ?>
			</div>

		</div>
	</div>
</div>

<?php get_footer(); ?>

