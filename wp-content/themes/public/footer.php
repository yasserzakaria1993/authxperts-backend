<div class="pop-left" style="background-color: <?php the_field('primary_color','option') ?>">
    <div class="content">
		<?php the_field('pop_right_text','option') ?>
    </div>
</div>
<div class="pop-left-open closed" style="background-color: <?php the_field('second__color_1','option') ?>">
    <i class="fas fa-bell"></i>
</div>


<div id="myModal" class="modal">
    <!-- Modal content -->
    <div class="modal-content auth-main-gradient-color">
        <div class="loader modal-loader">
            <img src="<?php the_image_src('loading.gif') ?>" alt="">
        </div>
        <span class="close"><i class="fal fa-times"></i></span>
        <div class="row">
            <div class="col-md-6" style="padding: 20px 10px;">
                <form class="" action="">
					<?php if (have_rows('countries','option')) : ?>
                        <div class="title auth-neutral-color">contact us</div>
                        <div class="input-group">
                            <label for="select-country">Select one Country</label>
                            <select name="select-country" id="select-country" class="countries in-modal">
								<?php while (have_rows('countries','option')) : the_row(); ?>
                                    <option data-text='<?php the_sub_field('country_information') ?>' data-map='<?php the_sub_field('country_map_frame') ?>' value="<?php echo strtolower( get_sub_field( 'country_name' ) ); ?>"><?php the_sub_field('country_name') ?></option>
								<?php endwhile; ?>
                            </select>

                        </div>
                        <div class="info">
	                        <?php while (have_rows('countries','option')) : the_row();
                                if (get_row_index() == 1){
	                                the_sub_field( 'country_information' );
                                }else{
                                    continue;
                                }
	                         endwhile; ?>
                        </div>
					<?php endif; ?>
                </form>
                <?php echo str_replace('”', '', do_shortcode('[contact-form-7 id="324" title="Untitled"]')); ?>
            </div>

            <div class="col-md-6 p-0">
                <div id="modal-map" style="height: 100%;">
	                <?php if (have_rows('countries','option')) : ?>
		                <?php while (have_rows('countries','option')) : the_row();
			                if (get_row_index() == 1){
				                the_sub_field( 'country_map_frame' );
			                }else{
				                continue;
			                }
		                endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>

        </div>

    </div>
</div>

<footer class="auth-footer" style="background-image: url(<?php the_image_src('footer-bg.png') ?>);">
<!--	<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="559" viewBox="0 0 1649 559"><g><g><path fill="#00dcaf" d="M1374 0h265c5.523 0 10 4.477 10 10v539c0 5.523-4.477 10-10 10h-265c-5.523 0-10-4.477-10-10V10c0-5.523 4.477-10 10-10z"/></g><g><path fill="#00dcaf" d="M1154 30h221c5.523 0 10 4.477 10 10v509c0 5.523-4.477 10-10 10h-221c-5.523 0-10-4.477-10-10V40c0-5.523 4.477-10 10-10z"/></g><g><path fill="#00dcaf" d="M934 61h221c5.523 0 10 4.477 10 10v478c0 5.523-4.477 10-10 10H934c-5.523 0-10-4.477-10-10V71c0-5.523 4.477-10 10-10z"/></g><g><path fill="#00dcaf" d="M715 89h221c5.523 0 10 4.477 10 10v450c0 5.523-4.477 10-10 10H715c-5.523 0-10-4.477-10-10V99c0-5.523 4.477-10 10-10z"/></g><g><path fill="#00dcaf" d="M497 61h221c5.523 0 10 4.477 10 10v478c0 5.523-4.477 10-10 10H497c-5.523 0-10-4.477-10-10V71c0-5.523 4.477-10 10-10z"/></g><g><path fill="#00dcaf" d="M274 30h221c5.523 0 10 4.477 10 10v509c0 5.523-4.477 10-10 10H274c-5.523 0-10-4.477-10-10V40c0-5.523 4.477-10 10-10z"/></g><g><path fill="#00dcaf" d="M10 0h265c5.523 0 10 4.477 10 10v539c0 5.523-4.477 10-10 10H10c-5.523 0-10-4.477-10-10V10C0 4.477 4.477 0 10 0z"/></g></g></svg>-->
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-4 col-sm-4 col-12">
				<img alt="Footer Logo" class="footer-logo" src="<?php the_field('footer_logo','option') ?>">

				<div class="footer-social-icons">
					<span class="the-title">Follow us on</span>
					<div class="the-icons">
						<a class="the-icon" href="<?php the_field('footer_facebook_link','option') ?>"><i class="fab fa-facebook hover-lighten"></i></a>
						<a class="the-icon" href="<?php the_field('footer_instagram_link','option') ?>"><i class="fab fa-instagram hover-lighten"></i></a>
						<a class="the-icon" href="<?php the_field('footer_twitter_link','option') ?>"><i class="fab fa-twitter hover-lighten"></i></a>
					</div>
				</div>

			</div>
			<div class="col-lg-6 col-md-8 col-sm-8 offset-md-0 offset-lg-3">
				<div class="row">
                    <?php if (have_rows('footer_menus','option')) : ?>
                        <?php while (have_rows('footer_menus','option')) : the_row(); ?>
					        <div class="col-md-4 col-sm-6">
                                <h5 class="footer-links-title"><?php the_sub_field('menu_name') ?></h5>
                                <div class="footer-links">
	                                <?php if (have_rows('menu_items','option')) : ?>
	                                    <?php while (have_rows('menu_items','option')) : the_row(); ?>
                                            <a class="footer-link" href="<?php echo get_sub_field('link')['url'] ?>"><?php echo get_sub_field('link')['title'] ?></a>
		                                <?php endwhile; ?>
	                                <?php endif ?>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php endif ?>

				</div>
			</div>

		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			<div class="d-flex">
				<div class="left-side">
					<img src="<?php the_field('footer_bottom_logo','option') ?>">
					<p><?php the_field('footer_bottom_text','option') ?></p>
					<a href="https://www.bbb.org/us/md/rockville/profile/legal-document-help/authxperts-llc-0241-145449112/#sealclick" target="_blank" rel="nofollow"><img src="https://seal-dc-easternpa.bbb.org/seals/blue-seal-250-52-bbb-145449112.png" style="border: 0;max-width: 75%;" alt="Authxperts LLC BBB Business Review" /></a>
				</div>
				<div class="right-side">
					<img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/cc-badges-ppmcvdam.png" alt="Buy now with PayPal" />
                    <img class="no-zoom" src="<?php the_image_src('wire_transfer.jpg') ?>">
<!--                    <img class="no-zoom" src="--><?php //the_image_src('payments/western-union.png') ?><!--">-->

                    <p class="copyright-text"><?php the_field('footer_bottom_copyright','option') ?></p>
                    <?php if (have_rows('footer_bottom_links','option') ) : ?>
                        <?php while (have_rows('footer_bottom_links','option')) : the_row(); ?>
                            <a href="<?php echo (get_sub_field('footer_bottom_link'))['url'] ?>"><?php echo (get_sub_field('footer_bottom_link'))['title'] ?></a>
                        <?php endwhile; ?>
                    <?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer()  ?>
<!--Start of Tawk.to Script-->
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5a7359614b401e45400c9894/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();

    (function ($) {
        // var fileWrap = $('.codedropz-upload-wrapper .codedropz-upload-handler .codedropz-btn-wrap a');
        // console.log(fileWrap);
        // fileWrap.find('a').remove();
    })(jQuery);
</script>
<!--End of Tawk.to Script-->
</body>
</html>